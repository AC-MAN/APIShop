package com.api.shop.config;

import com.api.shop.interceptor.CORSInterceptor;
import com.api.shop.interceptor.MyAuthInterceptor;
import com.api.shop.interceptor.MyUrlInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 注册拦截器
 */

@Configuration
public class RegisterInterceptor implements WebMvcConfigurer {

    /**
     * 将自定义拦截器作为 Bean 写入配置
     */
    @Bean
    public MyUrlInterceptor myUrlInterceptor(){
        return new MyUrlInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加顺序即拦截器的 preHandler() 方法执行顺序
        registry.addInterceptor(new CORSInterceptor());
        registry.addInterceptor(new MyUrlInterceptor());
        registry.addInterceptor(new MyAuthInterceptor());
    }
}
