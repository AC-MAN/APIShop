package com.api.shop.service;

import com.api.shop.bean.ApiMessage;
import com.api.shop.bean.ApiParam;
import com.api.shop.common.ReturnData;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-04 16:32
 * @Description: 普通用户 Service
 */
public interface GeneralUserService {

    /**
     * 新增 API
     */
    ReturnData addAPI(ApiMessage apiMessage, ArrayList<ApiParam> apiParamList, HttpServletRequest request);

    /**
     * 删除自己的 API
     */
    ReturnData deleteOwnAPI(Integer apiId, HttpServletRequest request);

    /**
     * 删除某一用户的 API
     */
    ReturnData deleteAPI(Integer userId, Integer apiId, HttpServletRequest request);

    /**
     * 修改 API
     */
    ReturnData exchangeAPI(ApiMessage apiMessage, ArrayList<ApiParam> apiParamList, HttpServletRequest request);

    /**
     * 查看自己全部的 API
     */
    ReturnData searchOwnAllAPIs(HttpServletRequest request);

    /**
     * 查看自己某种状态的 API
     * status: 对应 ApiStatus 枚举类
     */
    ReturnData searchOwnAPIsByStatus(Integer status, HttpServletRequest request);

    /**
     * 查看全部 API
     */
    ReturnData searchAllAPI(HttpServletRequest request);

    /**
     * 查看某种类型的全部 API
     */
    ReturnData searchOwnAPIsByType(String type, HttpServletRequest request);

    /**
     * 返回 API 的全部类型
     */
    ReturnData showAllType(HttpServletRequest request);

}
