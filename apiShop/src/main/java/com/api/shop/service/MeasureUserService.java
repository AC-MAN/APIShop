package com.api.shop.service;

import com.api.shop.bean.ApiTestParam;
import com.api.shop.common.ReturnData;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/6 15:11
 * @Description:
 */
public interface MeasureUserService {

    /**
     * 得到所有待测试的API
     */
    ReturnData getAllUnMeasureApi( HttpServletRequest request);

    /**
     * 获取某个api基本信息,参数列表及参数示例
     */

    ReturnData getOneApi(Integer apiId, HttpServletRequest request);


    /**
     * 功能测试
     * 返回params
     */
    ReturnData functionalTest(String url,ArrayList<ApiTestParam> apiTestParamArrayList, HttpServletRequest request);

    /**
     * 性能测试
     */
    ReturnData performanceTest(String url, ArrayList<ApiTestParam> paramArrayList, HttpServletRequest request);

    /**
     * 测试通过
     */
    ReturnData passTest(Integer userId, Integer apiId, HttpServletRequest request);

    /**
     * 测试不通过
     */

    ReturnData unPassTest(Integer userId, Integer apiId, HttpServletRequest request);

}
