package com.api.shop.service.impl;

import com.api.shop.bean.ApiMessage;
import com.api.shop.bean.ApiParam;
import com.api.shop.bean.ApiVO;
import com.api.shop.common.ReturnData;
import com.api.shop.dao.ApiDao;
import com.api.shop.service.GeneralUserService;
import com.api.shop.service.SysResourceManageService;
import com.api.shop.util.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-10 10:27
 * @Description:
 */
@Service
@Slf4j
public class SysResourceManageServiceImpl implements SysResourceManageService {

    @Resource
    private ApiDao apiDao;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private GeneralUserService generalUserService;

    @Override
    public ReturnData classifyAPI(Integer userId, Integer apiId, String type, HttpServletRequest request) {
        int line = apiDao.updateType(apiId, type);

        if (line == 1){

            Object object = redisUtil.getHash("api", "apiUserId_" + userId);
            ArrayList<ApiMessage> apiMessages;
            if(object != null){
                apiMessages = (ArrayList) object;
                for(ApiMessage message : apiMessages){
                    if(message.getApiId().equals(apiId)){
                        apiMessages.remove(message);
                        message.setApiType(type);
                        apiMessages.add(message);
                        redisUtil.setHash("api", "apiUserId_" + userId, apiMessages);
                        break;
                    }
                }
            }

            return ReturnData.success();
        }
        return ReturnData.fail("修改 API 失败，请重试");
    }

    @Override
    @Transactional
    public ReturnData batchDeleteAPI(ArrayList<ApiVO> apiIds, HttpServletRequest request) {
        for(ApiVO apiVO : apiIds){
//            apiDao.deleteAllParam(apiVO.getApiId());
//            apiDao.deleteMessage(apiVO.getApiId());
//            log.info("delete API: {}", apiVO);

            generalUserService.deleteAPI(apiVO.getUserId(), apiVO.getApiId(), request);
        }

        return ReturnData.success();
    }

    @Override
    public ReturnData showAPIByStatus(Integer status, HttpServletRequest request) {
        ArrayList<ApiMessage> apiMessages = apiDao.selectApiMessageByStatus(status);
        if(apiMessages == null || apiMessages.size() == 0){
            return ReturnData.fail("没有该状态的 API ");
        }
        for(ApiMessage message : apiMessages){
            ArrayList<ApiParam> apiParams = apiDao.selectParams(message.getApiId());
            message.setApiParamArrayList(apiParams);
        }
        return ReturnData.success(apiMessages);
    }

    @Override
    public ReturnData showAPIByType(String type, HttpServletRequest request) {
        ArrayList<ApiMessage> apiMessages = apiDao.selectApiMessageByType(type);
        if(apiMessages == null || apiMessages.size() == 0){
            return ReturnData.fail("没有该类型的 API ");
        }
        for(ApiMessage message : apiMessages){
            ArrayList<ApiParam> apiParams = apiDao.selectParams(message.getApiId());
            message.setApiParamArrayList(apiParams);
        }
        return ReturnData.success(apiMessages);
    }
}
