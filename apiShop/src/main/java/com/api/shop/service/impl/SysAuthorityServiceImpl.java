package com.api.shop.service.impl;

import com.api.shop.bean.RoleVO;
import com.api.shop.bean.SysPermission;
import com.api.shop.common.ReturnData;
import com.api.shop.dao.*;
import com.api.shop.exception.MyDataException;
import com.api.shop.service.SysAuthorityService;
import com.api.shop.util.BeanValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: wyx
 * @Date: 2019-04-07 20:08
 * @Description: 超级管理员权限管理
 */
@Service
@Slf4j
public class SysAuthorityServiceImpl implements SysAuthorityService {

    @Resource
    private SysPermissionDao sysPermissionDao;
    @Resource
    private UserRoleDao userRoleDao;
    @Resource
    private RolePermissionDao rolePermissionDao;

    @Override
    public ReturnData registerAuthority(SysPermission sysPermission, HttpServletRequest request) {
        BeanValidator.check(sysPermission);

        int line = sysPermissionDao.insert(sysPermission);
        if(line == 1){
            return ReturnData.success();
        }

        return ReturnData.fail("定义权限失败，请重试");
    }

    @Override
    public ReturnData exchangeAuthority(SysPermission sysPermission, HttpServletRequest request) {
        BeanValidator.check(sysPermission);

        int line = sysPermissionDao.update(sysPermission);
        if(line == 1){
            return ReturnData.success();
        }

        return ReturnData.fail("修改权限信息失败，请重试");
    }

    @Override
    public ReturnData showAllAuthority(HttpServletRequest request) {
        ArrayList<SysPermission> sysPermissions = sysPermissionDao.selectAll();
        if(sysPermissions == null || sysPermissions.isEmpty()){
            throw new MyDataException("没有权限存在");
        }
        return ReturnData.success(sysPermissions);
    }

    @Override
    @Transactional
    public ReturnData deleteAuthority(Integer permissionId, HttpServletRequest request) {
        rolePermissionDao.delete(permissionId);

        int line = sysPermissionDao.delete(permissionId);
        if(line == 1){
            return ReturnData.success();
        }

        return ReturnData.fail("删除权限信息失败，请重试");
    }

    @Override
    @Transactional
    public ReturnData bindRoleForAuthority(List<RoleVO> roleIdList, Integer permissionId, HttpServletRequest request) {
        rolePermissionDao.delete(permissionId);

        for(RoleVO roleVO : roleIdList){
            rolePermissionDao.insert(roleVO.getRoleId(), permissionId);
        }

        return ReturnData.success();
    }

    @Override
    public ReturnData grantAuthorityToUser(List<RoleVO> roleIdList, Integer userId, HttpServletRequest request) {
        userRoleDao.deleteByUserId(userId);

        for(RoleVO roleVO : roleIdList){
            userRoleDao.insertIntoRole(userId, roleVO.getRoleId());
        }

        return ReturnData.success();
    }

}
