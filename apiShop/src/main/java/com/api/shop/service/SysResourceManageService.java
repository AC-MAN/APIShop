package com.api.shop.service;

import com.api.shop.bean.ApiVO;
import com.api.shop.common.ReturnData;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-10 10:16
 * @Description: 超级管理员对资源的管理 Service
 */
public interface SysResourceManageService {

    /**
     *  修改 API 分类
     */
    ReturnData classifyAPI(Integer userId, Integer apiId, String type, HttpServletRequest request);

    /**
     * 批量删除 API
     */
    ReturnData batchDeleteAPI(ArrayList<ApiVO> apiIds, HttpServletRequest request);

    /**
     * 显示某一状态的全部 API
     */
    ReturnData showAPIByStatus(Integer status, HttpServletRequest request);

    /**
     * 显示某一类型的全部 API
     */
    ReturnData showAPIByType(String type, HttpServletRequest request);

}
