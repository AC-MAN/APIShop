package com.api.shop.service;

import com.api.shop.bean.SysUser;
import com.api.shop.common.ReturnData;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/3 14:20
 * @Description: 超级用户管理用户Service
 */
public interface SysUserManageService {


    /**
     * 管理员查看所有用户信息
     *
     */

    ReturnData showAll(HttpServletRequest request);
    /**
     * 管理员查看用户信息
     */
    ReturnData showUser(Integer userId, HttpServletRequest request);

    /**
     * 管理员添加用户
     */
    ReturnData registerUser(SysUser user, HttpServletRequest request);

    /**
     * 管理员修改用户
     */
    ReturnData updateUser(Integer userId,String nikeName, HttpServletRequest request);

    /**
     * 管理员删除用户
     */
    ReturnData deleteUser(Integer userId, HttpServletRequest request);

}
