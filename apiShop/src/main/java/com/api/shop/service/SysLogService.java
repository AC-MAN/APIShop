package com.api.shop.service;

import com.api.shop.bean.SysLog;
import com.api.shop.common.ReturnData;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/8 15:34
 * @Description: 日志Service
 */
public interface SysLogService {


    /**
     * 查看所有日志
     */

    ReturnData showAllLog();

    /**
     * 查看指定Id操作日志
     */
    ReturnData showLogByOperatorId(Integer operatorId);

    /**
     * 查看指定操作类型日志
     */

    ReturnData showLogByOperate(String operate);
}
