package com.api.shop.service.impl;

import com.api.shop.bean.SysLog;
import com.api.shop.common.ReturnData;
import com.api.shop.dao.SysLogDao;
import com.api.shop.exception.ParamException;
import com.api.shop.service.SysLogService;
import jdk.nashorn.internal.runtime.regexp.joni.constants.OPCode;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/8 15:37
 * @Description:
 */
@Service
public class SysLogServiceImpl  implements SysLogService {

    @Resource
    SysLogDao sysLogDao;


    @Override
    public ReturnData showAllLog() {

        ArrayList<SysLog> sysLogArrayList  = sysLogDao.selectAll();
        return ReturnData.success(sysLogArrayList);
    }

    @Override
    public ReturnData showLogByOperatorId(Integer operatorId) {

        if(operatorId == null){

            throw new ParamException("Id参数不能为空");
        }
        ArrayList<SysLog> sysLogArrayList = sysLogDao.selectByOperatorId(operatorId);
        return ReturnData.success(sysLogArrayList);
    }

    @Override
    public ReturnData showLogByOperate(String operate) {
        if(operate == null){

            throw new ParamException("操作名不能为空");
        }
        ArrayList<SysLog> sysLogArrayList = sysLogDao.selectByOperate(operate);
        return ReturnData.success(sysLogArrayList);
    }
}
