package com.api.shop.service;

import com.api.shop.bean.SysUser;
import com.api.shop.common.ReturnData;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: wyx
 * @Date: 2019-04-03 11:13
 * @Description: 用户相关操作 Service
 */

public interface UserService {

    /**
     * 用户注册
     */
    ReturnData register(SysUser sysUser, HttpServletRequest request);

    ReturnData upAvatar(MultipartFile avatarPicture,HttpServletRequest request);
    /**
     * 用户登录
     */
    ReturnData login(String username, String psw, HttpServletRequest request);

    /**
     * 发送验证码
     */
    ReturnData sendSMS(String phoneNumber);

    /**
     * 用户快速登录(手机验证码)
     *
     */

    ReturnData qkLogin(String phoneNumber,String code,HttpServletRequest request);
    /**
     * 用户注销
     */
    ReturnData logout(HttpServletRequest request);

    /**
     * 显示用户信息
     */
    ReturnData showMessage(HttpServletRequest request);

    /**
     * 修改用户信息
     */
    ReturnData exchangeMessage(String nickName, HttpServletRequest request);

    /**
     * 验证用户是否有权限访问 url
     */
    ReturnData validateAuth(String needAuthUrl, HttpServletRequest request);

}
