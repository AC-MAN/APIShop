package com.api.shop.service;

import com.api.shop.bean.SysRole;
import com.api.shop.common.ReturnData;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: wyx
 * @Date: 2019-04-03 21:13
 * @Description: 超级用户管理角色 Service
 */
public interface SysRoleManageService {

    /**
     * 添加角色
     */
    ReturnData addRole(SysRole sysRole, HttpServletRequest request);

    /**
     * 删除角色（超级管理员无法被删除）
     */
    ReturnData deleteRole(Integer roleId, HttpServletRequest request);

    /**
     * 查看所有角色
     */
    ReturnData showAllRole(HttpServletRequest request);

    /**
     * 通过 roleId 查找角色
     */
    ReturnData searchRoleById(Integer roleId, HttpServletRequest request);

    /**
     * 通过 roleName 查找角色
     */
    ReturnData searchRoleByName(String roleName, HttpServletRequest request);

    /**
     * 修改角色信息
     */
    ReturnData exchangeRole(SysRole sysRole, HttpServletRequest request);

    /**
     * 冻结角色（使角色无法使用）
     */
    ReturnData freezeRole(Integer roleId, HttpServletRequest request);

    /**
     * 激活角色（使角色可以重新使用）
     */
    ReturnData activateRole(Integer roleId, HttpServletRequest request);

}
