package com.api.shop.service;

import com.api.shop.bean.RoleVO;
import com.api.shop.bean.SysPermission;
import com.api.shop.common.ReturnData;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Auther: wyx
 * @Date: 2019-04-07 19:40
 * @Description: 超级管理员权限管理
 */
public interface SysAuthorityService {

    /**
     *  添加权限
     */
    ReturnData registerAuthority(SysPermission sysPermission, HttpServletRequest request);

    /**
     * 修改权限
     */
    ReturnData exchangeAuthority(SysPermission sysPermission, HttpServletRequest request);

    /**
     *  返回全部权限列表
     */
    ReturnData showAllAuthority(HttpServletRequest request);

    /**
     * 删除权限
     */
    ReturnData deleteAuthority(Integer permissionId, HttpServletRequest request);

    /**
     * 权限委托（为权限绑定角色）
     */
    ReturnData bindRoleForAuthority(List<RoleVO> roleIdList, Integer permissionId, HttpServletRequest request);

    /**
     * 权限委托（授予用户权限，即为用户绑定角色）
     */
    ReturnData grantAuthorityToUser(List<RoleVO> roleIdList, Integer userId, HttpServletRequest request);

}
