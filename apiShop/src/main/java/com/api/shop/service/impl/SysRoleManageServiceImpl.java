package com.api.shop.service.impl;

import com.api.shop.bean.SysRole;
import com.api.shop.common.ReturnData;
import com.api.shop.dao.SysRoleDao;
import com.api.shop.dao.UserRoleDao;
import com.api.shop.exception.MyDataException;
import com.api.shop.service.SysRoleManageService;
import com.api.shop.util.BeanValidator;
import com.api.shop.util.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-03 21:25
 * @Description:
 */
@Service
@Slf4j
public class SysRoleManageServiceImpl implements SysRoleManageService {

    @Resource
    private SysRoleDao sysRoleDao;
    @Resource
    private UserRoleDao userRoleDao;

    @Override
    public ReturnData addRole(SysRole sysRole, HttpServletRequest request) {
        if(!sysRole.getRoleName().contains("ROLE_")){
            sysRole.setRoleName("ROLE_" + sysRole.getRoleName());
        }
        BeanValidator.check(sysRole);

        if(sysRoleDao.selectByRoleName(sysRole.getRoleName()) != null){
            throw new MyDataException("该角色已存在");
        }

        int line = sysRoleDao.insert(sysRole);
        if(line != 1){
            throw new MyDataException("角色添加失败，请重试");
        }
        log.info("Admin:{} addRole: {} 成功", IpUtil.getIpAddr(request), sysRole.getRoleName());

        return ReturnData.success();
    }

    @Transactional
    @Override
    public ReturnData deleteRole(Integer roleId, HttpServletRequest request) {
        ReturnData returnData = searchRoleById(roleId, request);
        SysRole sysRole = (SysRole) returnData.getData();
        if(sysRole.getRoleName().equals("ROLE_ADMIN")){
            throw new MyDataException("超级管理员角色无法被删除");
        }

        sysRoleDao.delete(roleId);
        userRoleDao.deleteByRoleId(roleId);
        log.info("Admin:{} deleteRole: {} 成功", IpUtil.getIpAddr(request), sysRole.getRoleName());

        return ReturnData.success();
    }

    @Override
    public ReturnData showAllRole(HttpServletRequest request) {
        ArrayList<SysRole> sysRoles = sysRoleDao.selectAllRole();
        if(sysRoles == null || sysRoles.isEmpty()){
            throw new MyDataException("没有任何角色信息");
        }

        return ReturnData.success(sysRoles);
    }

    @Override
    public ReturnData searchRoleById(Integer roleId, HttpServletRequest request) {
        SysRole sysRole = sysRoleDao.selectByRoleId(roleId);
        if(sysRole == null){
            throw new MyDataException("该角色不存在");
        }

        return ReturnData.success(sysRole);
    }

    @Override
    public ReturnData searchRoleByName(String roleName, HttpServletRequest request) {
        SysRole sysRole = sysRoleDao.selectByRoleName(roleName);
        if(sysRole == null){
            throw new MyDataException("该角色不存在");
        }else{
            return ReturnData.success(sysRole);
        }
    }

    @Override
    public ReturnData exchangeRole(SysRole sysRole, HttpServletRequest request) {
        if(!sysRole.getRoleName().contains("ROLE_")){
            sysRole.setRoleName("ROLE_" + sysRole.getRoleName());
        }
        BeanValidator.check(sysRole);

        SysRole role = sysRoleDao.selectByRoleId(sysRole.getRoleId());
        if(role.getRoleName().equals("ROLE_ADMIN")){
            throw new MyDataException("超级管理员角色信息无法被修改");
        }

        role = sysRoleDao.selectByRoleName(sysRole.getRoleName());
        if(role != null && (role.getRoleId() != sysRole.getRoleId())){
            throw new MyDataException("该角色已存在");
        }

        sysRoleDao.update(sysRole);
        log.info("Admin:{} exchangeRole: {} 成功", IpUtil.getIpAddr(request), sysRole.getRoleName());

        return ReturnData.success();
    }

    @Override
    public ReturnData freezeRole(Integer roleId, HttpServletRequest request) {
        ReturnData returnData = searchRoleById(roleId, request);
        SysRole sysRole = (SysRole) returnData.getData();

        if(sysRole.getRoleName().equals("ROLE_ADMIN")){
            throw new MyDataException("超级管理员角色无法被冻结");
        }

        sysRoleDao.freeze(roleId);
        log.info("Admin:{} freezeRole: {} 成功", IpUtil.getIpAddr(request), sysRole.getRoleName());

        return ReturnData.success();
    }

    @Override
    public ReturnData activateRole(Integer roleId, HttpServletRequest request) {
        ReturnData returnData = searchRoleById(roleId, request);
        SysRole sysRole = (SysRole) returnData.getData();

        sysRoleDao.activate(roleId);
        log.info("Admin:{} activateRole: {} 成功", IpUtil.getIpAddr(request), sysRole.getRoleName());

        return ReturnData.success();
    }

}
