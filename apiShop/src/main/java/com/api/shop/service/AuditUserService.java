package com.api.shop.service;

import com.api.shop.common.ReturnData;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/5 20:20
 * @Description: 审核用户Service
 */
public interface AuditUserService {
    /**
     * 获得未审核的Api
     */
    public ReturnData getApiByStatus(Integer Status, HttpServletRequest request);


    /**
     * Api审核通过
     */
    public ReturnData passAuditApi(Integer userId, Integer apiId, HttpServletRequest request);

    /**
     * Api审核不通过
     */
    public ReturnData unPassAuditApi(Integer userId, Integer apiId, HttpServletRequest request);
}
