package com.api.shop.service.impl;

import com.api.shop.bean.ApiMessage;
import com.api.shop.bean.ApiParam;
import com.api.shop.common.ApiStatusEnum;
import com.api.shop.common.ReturnData;
import com.api.shop.dao.ApiDao;
import com.api.shop.exception.ParamException;
import com.api.shop.service.AuditUserService;
import com.api.shop.util.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/5 20:30
 * @Description:
 */
@Service
@Slf4j
public class AuditUserServiceImpl implements AuditUserService {

    @Resource
    private ApiDao apiDao;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public ReturnData getApiByStatus(Integer status, HttpServletRequest request) {
        if(status!= ApiStatusEnum.CHECK_PENDING.getValue() && status!=ApiStatusEnum.AUDIT_FAILURE.getValue())
        {
            log.error("Api状态参数格式不正确 {}",status);
            throw  new ParamException("Api状态参数格式不正确");
        }

        ArrayList<ApiMessage> apiMessageArrayList = apiDao.selectApiMessageByStatus(status);
        if(apiMessageArrayList!=null){

            for(ApiMessage apiMessage :apiMessageArrayList){

                ArrayList<ApiParam> apiParamArrayList = apiDao.selectParams(apiMessage.getApiId());
                apiMessage.setApiParamArrayList(apiParamArrayList);
            }

        }
        return ReturnData.success(apiMessageArrayList);
    }


    @Override
    public ReturnData passAuditApi(Integer userId, Integer apiId, HttpServletRequest request) {
        //当api状态为待审核和未通过时才能通过审核
        ApiMessage apiMessage = apiDao.selectMessage(apiId);
        Integer status = apiMessage.getApiStatus();

        log.debug("passAuditApi apiMessage: " + apiMessage);

        if(status != ApiStatusEnum.CHECK_PENDING.getValue() && status!=ApiStatusEnum.AUDIT_FAILURE.getValue())
        {
            log.error("该Api状态无法被审核{}",status);
            throw new ParamException("该Api状态无法被审核");
        }

        apiDao.updateStatus(apiMessage.getApiId(), ApiStatusEnum.TO_BE_TESTED.getValue());

        Object object = redisUtil.getHash("api", "apiUserId_" + userId);
        if(object != null){
            ArrayList<ApiMessage> apiMessages = (ArrayList) object;

            for(int i = 0; i < apiMessages.size(); i++){
                ApiMessage message = apiMessages.get(i);

                if(message.getApiId().equals(apiId)){
                    message.setApiStatus(ApiStatusEnum.TO_BE_TESTED.getValue());
                    apiMessages.set(i, message);

                    redisUtil.setHash("api", "apiUserId_" + userId, apiMessages);
                    log.info("Redis pass API success");
                    break;
                }
            }

        }

        return ReturnData.success();
    }

    @Override
    public ReturnData unPassAuditApi(Integer userId, Integer apiId, HttpServletRequest request) {

        //当api状态为待审核才能审核不通过
        ApiMessage apiMessage = apiDao.selectMessage(apiId);

        if(apiMessage.getApiStatus() != ApiStatusEnum.CHECK_PENDING.getValue()) {

            log.error("该Api状态无法审核不通过{}", apiMessage.getApiStatus());
            throw new ParamException("该Api状态无法审核不通过");
        }

        apiDao.updateStatus(apiMessage.getApiId(), ApiStatusEnum.AUDIT_FAILURE.getValue());

        Object object = redisUtil.getHash("api", "apiUserId_" + userId);
        if(object != null){
            ArrayList<ApiMessage> apiMessages = (ArrayList) object;

            for(int i = 0; i < apiMessages.size(); i++){
                ApiMessage message = apiMessages.get(i);

                if(message.getApiId().equals(apiId)){
                    message.setApiStatus(ApiStatusEnum.AUDIT_FAILURE.getValue());
                    apiMessages.set(i, message);

                    redisUtil.setHash("api", "apiUserId_" + userId, apiMessages);
                    log.info("Redis unPass API success");
                    break;
                }
            }

        }

        return ReturnData.success();
    }
}
