package com.api.shop.service.impl;

import com.api.shop.bean.SysUser;
import com.api.shop.common.ReturnData;
import com.api.shop.dao.SysUserDao;
import com.api.shop.dao.UserRoleDao;
import com.api.shop.exception.ParamException;
import com.api.shop.service.SysUserManageService;
import com.api.shop.util.BeanValidator;
import com.api.shop.util.fileUpload.FileUtils;
import com.api.shop.util.MD5Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/3 14:30
 * @Description: 超级用户对用户信息的管理
 */
@Service
@Slf4j
public class SysUserManageServiceImpl implements SysUserManageService {

    @Resource
    SysUserDao sysUserDao;
    @Resource
    UserRoleDao userRoleDao;

    @Override
    public ReturnData showAll(HttpServletRequest request) {

        ArrayList<SysUser> userArrayList = sysUserDao.selectAll();
        if(userArrayList == null){
            userArrayList = new ArrayList<>();
        }
        return ReturnData.success(userArrayList);
    }

    @Override
    public ReturnData showUser(Integer userId, HttpServletRequest request) {

        SysUser sysUser = sysUserDao.selectUserById(userId);
        if(sysUser == null) {

            log.error("【显示用户信息】 用户不存在");
            throw new ParamException("用户不存在"+userId);
        }
        return ReturnData.success(sysUser);
    }

    @Override
    public ReturnData registerUser(SysUser user, HttpServletRequest request) {

        BeanValidator.check(user);
        user.setAvatar("http://yjjvip.cn:8080/pictures/default.jpg");
        //查询有无重复用户名
        SysUser sysUser = sysUserDao.selectUserByName(user.getUsername());
        if(sysUser!=null){
            log.error("【添加用户】 用户名已存在");
            throw new RuntimeException("该用户: " + sysUser.getUsername() + " 已存在");
        }


        user.setPassword(MD5Util.getEncoderStr(user.getPassword()));

        int result1 = sysUserDao.insert(user);


        if(result1 ==1 ) {
            log.info(user.getUsername()+"加入成功");
            return ReturnData.success();
        }
        else{
            return ReturnData.fail("用户插入数据库失败");
        }

    }

    @Override
    public ReturnData updateUser(Integer userId,String nikeName, HttpServletRequest request) {

        SysUser sysUser = sysUserDao.selectUserById(userId);
        if(sysUser == null) {

            log.error("【更新用户信息】 用户不存在");
            throw new ParamException("用户不存在"+userId);
        }
        sysUser.setNickName(nikeName);
        int result1 = sysUserDao.update(sysUser);

        if(result1!=1 ){
            return ReturnData.fail("更新错误");
        }

       return ReturnData.success();

    }

    @Override
    @Transactional
    public ReturnData deleteUser(Integer userId, HttpServletRequest request) {

        SysUser sysUser = sysUserDao.selectUserById(userId);
        if(sysUser == null) {

            log.error("【删除用户】 用户不存在");
            throw new ParamException("用户不存在"+userId);
        }
        //在两个表中删除
        int result2 = userRoleDao.deleteByUserId(userId);
        int result1 = sysUserDao.delete(userId);

        if(result1 ==1 && result2==1){
            return ReturnData.success();
        }
        else{
            return ReturnData.fail("用户删除失败");
        }
    }

}
