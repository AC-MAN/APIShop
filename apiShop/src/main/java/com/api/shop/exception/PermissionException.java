package com.api.shop.exception;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 权限异常
 */

public class PermissionException extends RuntimeException {

    public PermissionException(){
        super();
    }

    public PermissionException(String message){
        super(message);
    }

    public PermissionException(Throwable throwable){
        super(throwable);
    }

    protected PermissionException(String message, Throwable throwable, boolean enableSuppression, boolean writeableStackTrace){
        super(message, throwable, enableSuppression, writeableStackTrace);
    }

}
