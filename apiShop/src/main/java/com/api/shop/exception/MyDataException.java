package com.api.shop.exception;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 数据操作异常
 */

public class MyDataException extends RuntimeException {

    public MyDataException(){
        super();
    }

    public MyDataException(String message){
        super(message);
    }

    public MyDataException(Throwable throwable){
        super(throwable);
    }

    protected MyDataException(String message, Throwable throwable, boolean enableSuppression, boolean writeableStackTrace){
        super(message, throwable, enableSuppression, writeableStackTrace);
    }

}
