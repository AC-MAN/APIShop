package com.api.shop.exception;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 参数异常
 */

public class ParamException extends RuntimeException {

    public ParamException(){
        super();
    }

    public ParamException(String message){
        super(message);
    }

    public ParamException(Throwable throwable){
        super(throwable);
    }

    protected ParamException(String message, Throwable throwable, boolean enableSuppression, boolean writeableStackTrace){
        super(message, throwable, enableSuppression, writeableStackTrace);
    }

}
