package com.api.shop.bean;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: API 信息类
 */

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ApiMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer apiId;

    @NotNull(message = "URL 不能为空")
    @Length(max = 100, message = "URL 的长度不能大于 100")
    private String apiUrl;

    @NotNull(message = "类型不能为空")
    @Length(max = 10, message = "类型长度不能大于 10")
    private String apiType;

    @NotNull(message = "状态不能为空")
    private Integer apiStatus;

    @NotNull(message = "所属用户不能为空")
    private Integer apiBelongUserId;

    @NotNull(message = "API 功能描述不能为空")
    @Length(max = 40, message = "API 功能描述长度不能大于 40")
    private String apiDescription;

    private ArrayList<ApiParam> apiParamArrayList;

}
