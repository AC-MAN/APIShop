package com.api.shop.bean;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 角色信息类
 */

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SysRole implements Serializable {

    private Integer roleId;

    /**
     *  ROLE_ADMIN: 超级管理员
     *  ROLE_AUDIT: 审核用户
     *  ROLE_MEASURE: 测试用户
     *  ROLE_GENERAL: 普通用户
     */
    @NotNull(message = "角色名不能为空")
    @Length(min = 5, max = 12, message = "角色名长度要在 5 到 12 之间")
    private String roleName;

    @NotNull(message = "角色描述不能为空")
    @Length(max = 60, message = "角色描述长度最大为 60")
    private String roleDescription;

    @NotNull(message = "不能为空")
    private Integer roleStatus;

}
