
package com.api.shop.bean;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 用户信息类
 */

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SysUser implements Serializable {

    private Integer userId;

    @NotNull(message = "用户名不能为空")
    @Length(min = 2, max = 20, message = "用户名长度要在 2 到 10 之间")
    private String username;

    @NotNull(message = "用户密码不能为空")
    @Length(min = 10, max = 15, message = "密码长度要在 10 到 15之间")
    private String password;

    @NotNull(message = "用户昵称不能为空")
    @Length(min = 2, max = 8, message = "昵称长度要在 2 到 8 之间")
    private String nickName;

//    @Length(max = 100, message = "头像 URL 的长度不可以超过 100")
    private String avatar;

}
