package com.api.shop.bean;

import lombok.*;

import java.io.Serializable;

/**
 * @Auther: wyx
 * @Date: 2019-04-07 20:38
 * @Description:
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RoleVO implements Serializable {

    private Integer roleId;

}
