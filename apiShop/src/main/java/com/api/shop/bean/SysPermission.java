package com.api.shop.bean;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 权限信息类
 */

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SysPermission implements Serializable {

    private Integer permissionId;

    @NotNull(message = "权限名不能为空")
    @Length(min = 2, max = 10, message = "权限名长度要在 2 到 10 之间")
    private String permissionName;

    @NotNull(message = "权限类型不能为空")
    @Length(max = 60, message = "权限类型长度不能超过 60")
    private String permissionDescription;

    @NotNull(message = "权限状态不能为空")
    private Integer permissionStatus;

    @NotNull(message = "权限可用 URL 不能为空")
    @Length(max = 100, message = "权限可用 URL 长度不能超过 100")
    private String permissionUrl;

}
