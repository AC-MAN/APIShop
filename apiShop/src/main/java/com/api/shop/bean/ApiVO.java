package com.api.shop.bean;

import lombok.*;

import java.io.Serializable;

/**
 * @Auther: wyx
 * @Date: 2019-04-10 10:31
 * @Description:
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ApiVO implements Serializable {

    private Integer apiId;

    private Integer userId;

}
