package com.api.shop.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/6 16:30
 * @Description: 仅测试api使用
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ApiTestParam {

    private String apiParamKey;

    private String apiParamValue;
}
