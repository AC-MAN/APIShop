package com.api.shop.bean;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: API 参数类
 */

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ApiParam implements Serializable {

    private Integer paramId;

    @NotNull(message = "所属 apiId 不能为空")
    private Integer apiId;

    @NotNull(message = "参数名不能为空")
    @Length(max = 20, message = "参数名长度不能超过 20")
    private String apiParamKey;

    @NotNull(message = "参数值不能为空")
    @Length(max = 30, message = "参数值长度不能超过 30")
    private String apiParamValue;

    @NotNull(message = "参数类型不能为空")
    @Length(max = 10, message = "参数类型长度不能超过 10")
    private String apiParamType;

}
