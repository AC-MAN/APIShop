package com.api.shop.bean;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 日志信息类
 */

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SysLog implements Serializable {

    private Integer logId;

    @NotNull(message = "日志时间不能为空")
    private Date logDateTime;

    @NotNull(message = "操作者 id 不能为空")
    private Integer logOperatorId;

    @NotNull(message = "操作者 ip 不能为空")
    @Length(min = 7, max = 15, message = "操作者 ip 长度要在 7 到 15 之间")
    private String logOperatorIp;

    @NotNull(message = "日志操作类型不能为空")
    @Length(max = 8, message = "日志操作类型长度不能超过 8")
    private String logOperate;

    @NotNull(message = "日志信息不能为空")
    @Length(max = 60, message = "日志信息长度不能超过 60")
    private String logMessage;

}
