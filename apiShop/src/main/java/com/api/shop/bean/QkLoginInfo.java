package com.api.shop.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/10 18:24
 * @Description: 快速登录（手机验证码）时保存的信息
 */
@Data

public class QkLoginInfo implements Serializable {

    /**
     * 验证码过期时间
     */
   public  final static long codeOutTime = 5*60*1000;
    /**
     * 验证码
     */
    private String code;
    /**
     * 验证码发送时间，用来校验验证码是否过期
     */
    private Date codeDate;
}
