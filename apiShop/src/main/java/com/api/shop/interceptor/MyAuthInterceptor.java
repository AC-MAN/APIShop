package com.api.shop.interceptor;

import com.api.shop.common.NoLoginUrl;
import com.api.shop.common.RequestHolder;
import com.api.shop.common.ReturnData;
import com.api.shop.exception.PermissionException;
import com.api.shop.service.UserService;
import com.api.shop.util.ClassUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @Auther: wyx
 * @Date: 2019-04-04 11:00
 * @Description: 通过权限拦截访问
 */

@Slf4j
public class MyAuthInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("this interceptor is MyAuthInterceptor");

        String method = request.getMethod();
        if(method.equals("OPTIONS")){
            return true;
        }

        UserService userService = ClassUtil.getClassT(UserService.class, request);

        String auth = getAuth(request);
        System.out.println("----->"+auth);
        if (auth.equals("NoPermissionsRequired")){
            return true;
        }

        ReturnData returnData = userService.validateAuth(auth, request);
        System.out.println("----->"+returnData.getMessage());
        System.out.println("----->"+returnData.isResult());
        if(!returnData.isResult()){
            throw new PermissionException(returnData.getMessage());
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        RequestHolder.remove();
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        RequestHolder.remove();
    }

    private String getAuth(HttpServletRequest request){
        String url = request.getRequestURI().trim();
        log.info("request url: {}", url);

        Map<String, Object> map = NoLoginUrl.getMap();
        if(map != null){
            for(Map.Entry<String, Object> entry : map.entrySet()){
                log.info("map: key: {}, value:{}", entry.getKey(), entry.getValue());
                if(url.contains(String.valueOf(entry.getValue()))){
                    return "NoPermissionsRequired";
                }
            }
        }

        String[] strings = url.split("/");
        int size = strings.length;
        log.info("AuthInterceptor url str length: {}", size);
//        if(size == 4){
//            //    /apiShop/user/showMessage ==> //user
//            return "/" + strings[2];
//        }else{
//            // /apiShop/admin/user/add ==> /admin/user
//            return "/" + strings[2] + "/" + strings[3];
//        }

        if(size == 3){
            //    /user/showMessage ==> //user
            return "/" + strings[1];
        }else{
            // /admin/user/add ==> /admin/user
            return "/" + strings[1] + "/" + strings[2];
        }

    }

}
