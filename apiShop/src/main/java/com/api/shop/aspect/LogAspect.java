package com.api.shop.aspect;
import com.api.shop.bean.*;
import com.api.shop.common.ReturnData;
import com.api.shop.dao.SysLogDao;
import com.api.shop.exception.MyDataException;
import com.api.shop.util.LogUtil;
import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;


/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/8 16:16
 * @Description: 超级用户用户管理日志
 */

@Aspect
@Component
public class LogAspect {

    private String operateName = null;

    private String detailMessage = null;
    @Resource
    SysLogDao sysLogDao;

    @AfterReturning(value = "com.api.shop.aspect.PointCuts.SysUserManageLogCut()", returning = "object")
    public void SysUserManageLog(JoinPoint joinPoint, Object object) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        SysUser sysUser = new SysUser();
        Object[] objects = joinPoint.getArgs();
        String methodName = joinPoint.getSignature().getName();
        if (objects[0] instanceof SysUser) {
            sysUser = (SysUser) objects[0];
        }
        switch (methodName) {
            case "registerUser":
                operateName = "超级用户添加用户";
                detailMessage = "用户名" + sysUser.getUsername();
                break;
            case "updateUser":
                operateName = "超级用户更新用户信息";
                detailMessage = "用户名" + sysUser.getUsername();
                break;
            case "deleteUser":
                operateName = "超级用户删除用户信息";
                detailMessage = "用户Id: " + objects[0];
                break;
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage, ((ReturnData) object).isResult(), operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception e){
//            throw new MyDataException("日志插入异常");
        }
    }

    @AfterReturning(value = "com.api.shop.aspect.PointCuts.AuditUserLogCut()", returning = "object")
    public void AuditLog(JoinPoint joinPoint, Object object) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Object[] objects = joinPoint.getArgs();
        Integer apiId = (Integer) objects[0];
        String methodName = joinPoint.getSignature().getName();
        switch (methodName) {
            case "passAuditApi":
                operateName = "api审核通过";
                detailMessage = "apiId :" + apiId;
                break;
            case "unPassAuditApi":
                operateName = "api审核不通过";
                detailMessage = "apiId : " + apiId;
                break;
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage, ((ReturnData) object).isResult(), operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception e){
//            throw new MyDataException("日志插入异常");
        }
    }

    @AfterReturning(value = "com.api.shop.aspect.PointCuts.MeasureUserLogCut()", returning = "object")
    public void measureLog(JoinPoint joinPoint, Object object) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Object[] objects = joinPoint.getArgs();
        String methodName = joinPoint.getSignature().getName();
        switch (methodName) {

            case "functionalTest":
                operateName = "api功能测试";
                detailMessage = "apiUrl: " + objects[0];
                break;
            case "performanceTest":
                operateName = "api性能测试";
                detailMessage = "apiUrl:" + objects[0];
            case "passTest":
                operateName = "api测试通过";
                detailMessage = "apiId: " + objects[0];
                break;
            case "unPassTest":
                operateName = "api测试不通过";
                detailMessage = "apiId: " + objects[0];
                break;
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage, ((ReturnData) object).isResult(), operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception e){
//            throw new MyDataException("日志插入异常");
        }

    }

    @AfterReturning(value = "com.api.shop.aspect.PointCuts.RoleManageLogCut()",returning = "object")
    public void RoleManageLog(JoinPoint joinPoint,Object object){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Object[] objects = joinPoint.getArgs();
        String methodName = joinPoint.getSignature().getName();

        switch (methodName){

            case "addRole":
                operateName = "添加角色";
                detailMessage = "角色名 :"+((SysRole)objects[0]).getRoleName();
                break;
            case "deleteRole":
                operateName = "删除角色";
                detailMessage = "角色Id :"+objects[0];
                break;
            case "exchangeRole":
                operateName = "更新角色";
                detailMessage = "角色Id : "+((SysRole)objects[0]).getRoleId();
                break;
            case "freezeRole":
                operateName = "冻结角色";
                detailMessage = "角色Id : " +objects[0];
                break;
            case  "activateRole":
                operateName = "激活角色";
                detailMessage = "角色Id : "+ objects[0];
                break;
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage,((ReturnData)object).isResult(),operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception e){
//            throw new MyDataException("日志插入异常");
        }

    }

    @AfterReturning(value="com.api.shop.aspect.PointCuts.AuthorityServiceLog()",returning = "object")
    public void AuthorityLog(JoinPoint joinPoint,Object object){
        Object[] objects = joinPoint.getArgs();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String methodName = joinPoint.getSignature().getName();

        switch (methodName){

            case "registerAuthority":
                operateName = "添加权限";
                detailMessage = "权限名 :"+((SysPermission)objects[0]).getPermissionName();
                break;
            case "deleteAuthority":
                operateName = "删除权限";
                detailMessage = "权限Id :"+objects[0];
                break;
            case "exchangeAuthority":
                operateName = "更新权限";
                detailMessage = "权限Id : "+((SysPermission)objects[0]).getPermissionId();
                break;
            case "bindRoleForAuthority":
                operateName = "角色授权";
                List<RoleVO> roleVOList = (List<RoleVO>)objects[0];
                detailMessage = "将权限Id为"+objects[1]+"的权限授予给角色(Id)" ;
                for(RoleVO roleVO : roleVOList){
                    detailMessage += roleVO.getRoleId()+",";
                }
                break;

            case  "grantAuthorityToUser":
                operateName = "用户授权";
                detailMessage = "角色Id : "+ objects[0];
                List<RoleVO> roleVOList1 = (List<RoleVO>)objects[0];
                detailMessage = "给Id为"+objects[1]+"的用户赋予角色(Id)" ;
                for(RoleVO roleVO : roleVOList1){
                    detailMessage += roleVO.getRoleId()+",";
                }
                break;
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage,((ReturnData)object).isResult(),operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception e){
//            throw new MyDataException("日志插入异常");
        }
    }

    @AfterReturning(value = "com.api.shop.aspect.PointCuts.UserLoginLog()",returning = "object")
    public void userLogigLog(JoinPoint joinPoint,Object object){

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Object[] objects = joinPoint.getArgs();
        String methodName = joinPoint.getSignature().getName();
        switch (methodName){

            case "login":
                operateName = "登录";
                detailMessage = "用户: "+objects[0]+"登录";
                break;
            case "qkLogin":
                operateName = "手机快速登录";
                detailMessage = "手机号为: "+objects[0]+"的用户登录";
                break;
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage,((ReturnData)object).isResult(),operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception e){
//            throw new MyDataException("日志插入异常");
        }

    }

    @AfterThrowing(value = "com.api.shop.aspect.PointCuts.exceptionLog()",throwing = "e")
    public void exceptionLog(JoinPoint joinPoint, Exception e){

        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        String methodName = joinPoint.getSignature().getName();
        operateName = methodName;
        if(e.getMessage()!=null){
            detailMessage = "发生异常"+e.getMessage();
        }
        else{
            detailMessage = "发生异常 : 未知异常";
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage,false,operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception err){
//            throw new MyDataException("日志插入异常");
        }
    }

    @AfterThrowing(value = "com.api.shop.aspect.PointCuts.exceptionLog()",throwing = "e")
    public void errorLog(JoinPoint joinPoint,Error e){

        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        String methodName = joinPoint.getSignature().getName();
        operateName = methodName;
        if(e.getMessage()!=null){
            detailMessage = "发生错误 : "+e.getMessage();
        }
        else{
            detailMessage = "发生错误 : 未知错误";
        }

        try {
            SysLog sysLog = LogUtil.setLog(detailMessage,false,operateName,request.getRemoteAddr());
            sysLogDao.insert(sysLog);
        }catch (Exception err){
//            throw new MyDataException("日志插入异常");
        }
    }
}

