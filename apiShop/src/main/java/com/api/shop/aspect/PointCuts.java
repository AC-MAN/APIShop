package com.api.shop.aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/8 15:31
 * @Description: 切点
 */

public class PointCuts {

    //超级用户管理用户日志切点
    @Pointcut("execution(public * com.api.shop.service.SysUserManageService.registerUser(..))"
            +"||execution(public * com.api.shop.service.SysUserManageService.updateUser(..)) "
            +"|| execution(public * com.api.shop.service.SysUserManageService.deleteUser(..))")
    public void SysUserManageLogCut(){}

    //审核用户日志切点
    @Pointcut("execution(public * com.api.shop.service.AuditUserService.passAuditApi(..))" +
            "||execution(public * com.api.shop.service.AuditUserService.passAuditApi(..))" +
            "|| execution(public * com.api.shop.service.AuditUserService.unPassAuditApi(..))")
    public void AuditUserLogCut(){}

    //测试用户日志切点
    @Pointcut("execution(public * com.api.shop.service.MeasureUserService.functionalTest(..))" +
            "||execution(public * com.api.shop.service.MeasureUserService.performanceTest(..))" +
            "||execution(public * com.api.shop.service.MeasureUserService.passTest(..))" +
            "||execution(public * com.api.shop.service.MeasureUserService.unPassTest(..))")
    public void MeasureUserLogCut(){}

    //角色管理日志切点
    @Pointcut("execution(public * com.api.shop.service.SysRoleManageService.addRole(..))" +
            "||execution(public * com.api.shop.service.SysRoleManageService.deleteRole(..))" +
            "||execution(public * com.api.shop.service.SysRoleManageService.exchangeRole(..))" +
            "||execution(public * com.api.shop.service.SysRoleManageService.freezeRole(..))" +
            "||execution(public * com.api.shop.service.SysRoleManageService.activateRole(..))")
    public void RoleManageLogCut(){}

    //权限管理日志切点
    @Pointcut("execution(public * com.api.shop.service.SysAuthorityService.*(..))")
    public void AuthorityServiceLog(){}

    //用户登录日志
    @Pointcut("execution(public * com.api.shop.service.UserService.login(..))" +
            "||execution(public * com.api.shop.service.UserService.qkLogin(..))")
    public void UserLoginLog(){}

    //异常日志
    @Pointcut("execution(public * com.api.shop.service.*.*(..))")
    public void exceptionLog(){}
}
