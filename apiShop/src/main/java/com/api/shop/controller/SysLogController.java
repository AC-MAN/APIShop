package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.common.ReturnData;
import com.api.shop.service.SysLogService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/9 16:20
 * @Description: 日志管理Controller
 */
@RestController
@RequestMapping("/admin/log")
public class SysLogController {

    @Resource
    SysLogService sysLogService;

    //查看所有的操作日志
    @PostMapping("/showAll")
    public ReturnData showAll(HttpServletRequest request){

        return sysLogService.showAllLog();
    }

    //查看某一个用户的操作日志
    @PostMapping("/showByOperatorId")
    public ReturnData showByOperatorId(@RequestBody JSONObject jsonObject){
        Integer operatorId = jsonObject.getInteger("operatorId");
        return sysLogService.showLogByOperatorId(operatorId);
    }

    //查看某一类操作的操作日志
    @PostMapping("/showByOperate")
    public ReturnData showByOperate(@RequestBody JSONObject jsonObject){
        String operate = jsonObject.getString("operate");
        return sysLogService.showLogByOperate(operate);
    }

}
