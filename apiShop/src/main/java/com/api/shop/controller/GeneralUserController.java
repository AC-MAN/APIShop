package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.ApiMessage;
import com.api.shop.bean.ApiParam;
import com.api.shop.bean.ApiTestParam;
import com.api.shop.common.ApiStatusEnum;
import com.api.shop.common.ReturnData;
import com.api.shop.service.GeneralUserService;
import com.api.shop.service.MeasureUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Map;

/**
 * @Auther: wyx
 * @Date: 2019-04-05 21:33
 * @Description:
 */

@RestController
@RequestMapping("/general")
public class GeneralUserController {

    @Resource
    private GeneralUserService generalUserService;
    @Resource
    MeasureUserService measureUserService;

    @PostMapping(value = "/addAPI")
    public ReturnData addAPI(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        System.out.println("--------->in controller---------------");
        ApiMessage apiMessage = new ApiMessage();
        String apiUrl = jsonObject.getString("apiUrl");
        String apiType = jsonObject.getString("apiType");
        String apiDescription = jsonObject.getString("apiDescription");
        apiMessage.setApiUrl(apiUrl);
        apiMessage.setApiType(apiType);
        apiMessage.setApiDescription(apiDescription);

        System.out.println(jsonObject.get("apiParams"));
        ArrayList<Map<String,String>> maps = (ArrayList<Map<String,String>>) jsonObject.get("apiParams");
        ArrayList<ApiParam> apiParamArrayList = new ArrayList<>();
        for(Map<String,String> params : maps){
            ApiParam apiParam = new ApiParam();
            for(Map.Entry entry : params.entrySet()){
                System.out.println("----------->key:"+entry.getKey()+"value:"+entry.getValue());
                if(entry.getKey().equals("apiParamKey")){
                    apiParam.setApiParamKey(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamValue")){
                    apiParam.setApiParamValue(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamType")){
                    apiParam.setApiParamType(String.valueOf(entry.getValue()));
                }
            }
            System.out.println("-----> " + apiParam);
            apiParamArrayList.add(apiParam);
        }

        return generalUserService.addAPI(apiMessage, apiParamArrayList, request);
    }

    @PostMapping(value = "/deleteAPI")
    public ReturnData deleteAPI(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer apiId = jsonObject.getInteger("apiId");
        return generalUserService.deleteOwnAPI(apiId, request);
    }

    @PostMapping(value = "/showAllType")
    public ReturnData showAllType(HttpServletRequest request){
        return generalUserService.showAllType(request);
    }

    @PostMapping(value = "/exchangeAPI")
    public ReturnData exchangeAPI(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        ApiMessage apiMessage = new ApiMessage();
        Integer apiId = jsonObject.getInteger("apiId");
        String apiUrl = jsonObject.getString("apiUrl");
        String apiType = jsonObject.getString("apiType");
        String apiDescription = jsonObject.getString("apiDescription");
        apiMessage.setApiId(apiId);
        apiMessage.setApiUrl(apiUrl);
        apiMessage.setApiType(apiType);
        apiMessage.setApiDescription(apiDescription);

        System.out.println(jsonObject.get("apiParams"));
        ArrayList<Map<String,String>> maps = (ArrayList<Map<String,String>>) jsonObject.get("apiParams");
        ArrayList<ApiParam> apiParamArrayList = new ArrayList<>();
        for(Map<String,String> params : maps){
            ApiParam apiParam = new ApiParam();
            for(Map.Entry entry : params.entrySet()){
                System.out.println("----------->key:"+entry.getKey()+"value:"+entry.getValue());
                if(entry.getKey().equals("apiParamKey")){
                    apiParam.setApiParamKey(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamValue")){
                    apiParam.setApiParamValue(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamType")){
                    apiParam.setApiParamType(String.valueOf(entry.getValue()));
                }
            }
            System.out.println("-----> " + apiParam);
            apiParamArrayList.add(apiParam);
        }

        return generalUserService.exchangeAPI(apiMessage, apiParamArrayList, request);
    }

    @PostMapping(value = "/searchOwnAllAPIs")
    public ReturnData searchOwnAllAPIs(HttpServletRequest request){
        return generalUserService.searchOwnAllAPIs(request);
    }

    @PostMapping(value = "/searchOwnAPIsByStatus")
    public ReturnData searchOwnAPIsByStatus(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer status = jsonObject.getInteger("status");
        return generalUserService.searchOwnAPIsByStatus(status, request);
    }

    @PostMapping(value = "/funcTest")
    public ReturnData funcTest(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String url = jsonObject.getString("url");

        System.out.println(jsonObject.get("apiParamArrayList"));
        ArrayList<Map<String,String>> maps = (ArrayList<Map<String,String>>) jsonObject.get("apiParamArrayList");
        ArrayList<ApiTestParam> apiParamArrayList = new ArrayList<>();
        for(Map<String,String> params : maps){
            ApiTestParam apiTestParam = new ApiTestParam();
            for(Map.Entry entry : params.entrySet()){
                System.out.println("----------->key:"+entry.getKey()+"value:"+entry.getValue());
                if(entry.getKey().equals("apiParamKey")){
                    apiTestParam.setApiParamKey(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamValue")){
                    apiTestParam.setApiParamValue(String.valueOf(entry.getValue()));
                }
            }
            System.out.println("-----> " + apiTestParam);
            apiParamArrayList.add(apiTestParam);
        }

        return measureUserService.functionalTest(url,apiParamArrayList,request);
    }

    @PostMapping(value = "/searchAllAPI")
    public ReturnData searchAllAPI(HttpServletRequest request){
        return generalUserService.searchAllAPI(request);
    }

    @PostMapping(value = "/searchOwnAPIsByType")
    public ReturnData searchOwnAPIsByType(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String type = jsonObject.getString("type");
        return generalUserService.searchOwnAPIsByType(type, request);
    }

}
