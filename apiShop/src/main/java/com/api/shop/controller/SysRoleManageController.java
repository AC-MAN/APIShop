package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.SysRole;
import com.api.shop.common.ReturnData;
import com.api.shop.service.SysRoleManageService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: wyx
 * @Date: 2019-04-03 23:02
 * @Description: 超级用户管理角色 Controller
 */

@RestController
@RequestMapping("/admin/role")
public class SysRoleManageController {

    @Resource
    private SysRoleManageService sysRoleManageService;

    @PostMapping(value = "/addRole")
    public ReturnData addRole(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String roleName = jsonObject.getString("roleName");
        String roleDescription = jsonObject.getString("roleDescription");
        Integer roleStatus = jsonObject.getInteger("roleStatus");
        SysRole sysRole = new SysRole();
        sysRole.setRoleName(roleName);
        sysRole.setRoleDescription(roleDescription);
        sysRole.setRoleStatus(roleStatus);
        return sysRoleManageService.addRole(sysRole, request);
    }

    @PostMapping(value = "/showAllRole")
    public ReturnData showAllRole(HttpServletRequest request){
        return sysRoleManageService.showAllRole(request);
    }

    @PostMapping(value = "/deleteRole")
    public ReturnData deleteRole(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer roleId = jsonObject.getInteger("roleId");
        return sysRoleManageService.deleteRole(roleId, request);
    }

    @PostMapping(value = "/searchRoleById")
    public ReturnData searchRoleById(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer roleId = jsonObject.getInteger("roleId");
        return sysRoleManageService.searchRoleById(roleId, request);
    }

    @PostMapping(value = "/searchRoleByName")
    public ReturnData searchRoleByName(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String roleName = jsonObject.getString("roleName");
        return sysRoleManageService.searchRoleByName(roleName, request);
    }

    @PostMapping(value = "/exchangeRole")
    public ReturnData exchangeRole(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer roleId = jsonObject.getInteger("roleId");
        String roleName = jsonObject.getString("roleName");
        String roleDescription = jsonObject.getString("roleDescription");
        Integer roleStatus = jsonObject.getInteger("roleStatus");
        SysRole sysRole = new SysRole(roleId, roleName, roleDescription, roleStatus);
        return sysRoleManageService.exchangeRole(sysRole, request);
    }

    @PostMapping(value = "/freezeRole")
    public ReturnData freezeRole(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer roleId = jsonObject.getInteger("roleId");
        return sysRoleManageService.freezeRole(roleId, request);
    }

    @PostMapping(value = "/activateRole")
    public ReturnData activateRole(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer roleId = jsonObject.getInteger("roleId");
        return sysRoleManageService.activateRole(roleId, request);
    }

}
