package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.RoleVO;
import com.api.shop.bean.SysPermission;
import com.api.shop.common.ReturnData;
import com.api.shop.service.SysAuthorityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Auther: wyx
 * @Date: 2019-04-07 20:24
 * @Description: 超级管理员权限管理
 */
@RestController
@RequestMapping("/admin/auth")
@Slf4j
public class SysAuthorityController {

    @Resource
    private SysAuthorityService sysAuthorityService;

    @PostMapping(value = "/registerAuthority")
    public ReturnData registerAuthority(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        SysPermission sysPermission = new SysPermission();
        String permissionName = jsonObject.getString("permissionName");
        String permissionDescription = jsonObject.getString("permissionDescription");
        Integer permissionStatus = jsonObject.getInteger("permissionStatus");
        String permissionUrl = jsonObject.getString("permissionUrl");
        sysPermission.setPermissionName(permissionName);
        sysPermission.setPermissionDescription(permissionDescription);
        sysPermission.setPermissionStatus(permissionStatus);
        sysPermission.setPermissionUrl(permissionUrl);
        return sysAuthorityService.registerAuthority(sysPermission, request);
    }

    @PostMapping(value = "/exchangeAuthority")
    public ReturnData exchangeAuthority(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer permissionId = jsonObject.getInteger("permissionId");
        String permissionName = jsonObject.getString("permissionName");
        String permissionDescription = jsonObject.getString("permissionDescription");
        Integer permissionStatus = jsonObject.getInteger("permissionStatus");
        String permissionUrl = jsonObject.getString("permissionUrl");
        SysPermission sysPermission = new SysPermission(permissionId, permissionName, permissionDescription, permissionStatus, permissionUrl);
        return sysAuthorityService.exchangeAuthority(sysPermission, request);
    }

    @PostMapping(value = "/showAllAuthority")
    public ReturnData showAllAuthority(HttpServletRequest request){
        return sysAuthorityService.showAllAuthority(request);
    }

    @PostMapping(value = "/deleteAuthority")
    public ReturnData deleteAuthority(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer permissionId = jsonObject.getInteger("permissionId");
        return sysAuthorityService.deleteAuthority(permissionId, request);
    }

    @PostMapping(value = "/bindRoleForAuthority")
    public ReturnData bindRoleForAuthority(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        List<RoleVO> roleIdList = (List<RoleVO>) jsonObject.get("roleIdList");
        Integer permissionId = jsonObject.getInteger("permissionId");
        return sysAuthorityService.bindRoleForAuthority(roleIdList, permissionId, request);
    }

    @PostMapping(value = "/grantAuthorityToUser")
    public ReturnData grantAuthorityToUser(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        List<RoleVO> roleIdList = (List<RoleVO>) jsonObject.get("roleIdList");
        Integer userId = jsonObject.getInteger("userId");
        return sysAuthorityService.grantAuthorityToUser(roleIdList, userId, request);
    }


}
