package com.api.shop.controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/6 16:10
 * @Description: 本地提供的REST服务
 */
@RestController
public class ApiRepository {

    @PostMapping("/math/add")
    public String add(String count1, String count2){
        Double c1 = Double.parseDouble(count1);
        Double  c2 = Double.parseDouble(count2);
        Double sum = c1 + c2;
        return count1 + " + " + count2 + " = " + String.format("%.2f", sum);
    }

    @PostMapping("/math/mul")
    public String mul(String count1, String count2){

        Double c1 = Double.parseDouble(count1);
        Double  c2 = Double.parseDouble(count2);
        Double sum = c1 * c2;
        return count1+ " * " + count2 + " = " + String.format("%.2f", sum);
    }

    @PostMapping("/math/sub")
    public String sub(String count1, String count2){

        Double c1 = Double.parseDouble(count1);
        Double  c2 = Double.parseDouble(count2);
        Double sum = c1 - c2;
        return count1 + " - " + count2 + " = " + String.format("%.2f", sum);
    }

    @PostMapping("/math/div")
    public String div(String count1, String count2){

        int c1 = Integer.parseInt(count1);
        int  c2 = Integer.parseInt(count2);
        if(c2 == 0){
            return "除数不能为零。";
        }

        double sum = (c1 * 1.0) / c2;
        return count1+ " - " +count2 + " = " + String.format("%.2f", sum);

    }
}
