package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.SysUser;
import com.api.shop.common.ReturnData;
import com.api.shop.service.UserService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 用户基本操作
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping(value = "/register")
    public ReturnData register(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        SysUser sysUser = new SysUser();
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        String nickName = jsonObject.getString("nickName");
        sysUser.setUsername(username);
        sysUser.setPassword(password);
        sysUser.setNickName(nickName);
        return userService.register(sysUser, request);
    }

    @PostMapping(value = "/upAvatar")
    public ReturnData upAvatar(@RequestParam(value = "avatarPicture") MultipartFile avatarPicture,HttpServletRequest request){
        return userService.upAvatar(avatarPicture,request);
    }

    @PostMapping(value = "/login")
    public ReturnData login(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        return userService.login(username, password, request);
    }

    @PostMapping(value = "/sendSMS")
    public ReturnData sendSMS(@RequestBody JSONObject jsonObject){
        String phoneNumber = jsonObject.getString("phoneNumber");
        return userService.sendSMS(phoneNumber);
    }

    @PostMapping(value = "/qkLogin")
    public ReturnData qkLogin(@RequestBody JSONObject jsonObject,HttpServletRequest request){
        String phoneNumber = jsonObject.getString("phoneNumber");
        String code = jsonObject.getString("code");
        return userService.qkLogin(phoneNumber,code,request);
    }
    @PostMapping(value = "/logout")
    public ReturnData logout(HttpServletRequest request){
        return userService.logout(request);
    }

    @PostMapping(value = "/showMessage")
    public ReturnData showUserMessage(HttpServletRequest request){
        return userService.showMessage(request);
    }

    @PostMapping(value = "/exchangeMessage")
    public ReturnData exchangeMessage(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String nickName = jsonObject.getString("nickName");
        return userService.exchangeMessage(nickName, request);
    }

}
