package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.ApiParam;
import com.api.shop.bean.ApiTestParam;
import com.api.shop.common.NoLoginUrl;
import com.api.shop.common.ReturnData;
import com.api.shop.service.MeasureUserService;
import com.api.shop.util.SendPostRequestUtil;
import com.sun.org.apache.regexp.internal.RE;
import org.hibernate.loader.custom.Return;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/6 15:10
 * @Description: 测试用户Controller
 */
@RestController
@RequestMapping("/measure")
public class MeasureUserController {

    @Resource
    MeasureUserService measureUserService;

    //获取待测试的api
    @PostMapping("/getAll")
    public ReturnData getAll(HttpServletRequest request){

        return measureUserService.getAllUnMeasureApi(request);
    }

    /**
     *功能测试
     * @return
     */
    //功能测试
    @PostMapping("/functionTest")
    public ReturnData functionTest(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String url = jsonObject.getString("url");

        System.out.println(jsonObject.get("apiParamArrayList"));
        ArrayList<Map<String,String>> maps = (ArrayList<Map<String,String>>) jsonObject.get("apiParamArrayList");
        ArrayList<ApiTestParam> apiParamArrayList = new ArrayList<>();
        for(Map<String,String> params : maps){
            ApiTestParam apiTestParam = new ApiTestParam();
            for(Map.Entry entry : params.entrySet()){
                System.out.println("----------->key:"+entry.getKey()+"value:"+entry.getValue());
                if(entry.getKey().equals("apiParamKey")){
                    apiTestParam.setApiParamKey(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamValue")){
                    apiTestParam.setApiParamValue(String.valueOf(entry.getValue()));
                }
            }
            System.out.println("-----> " + apiTestParam);
            apiParamArrayList.add(apiTestParam);
        }

        return measureUserService.functionalTest(url,apiParamArrayList,request);
    }

    //性能测试
    @PostMapping("/performanceTest")
    public ReturnData performanceTest(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String url = jsonObject.getString("url");

        System.out.println(jsonObject.get("apiParamArrayList"));
        ArrayList<Map<String,String>> maps = (ArrayList<Map<String,String>>) jsonObject.get("apiParamArrayList");
        ArrayList<ApiTestParam> apiParamArrayList = new ArrayList<>();
        for(Map<String,String> params : maps){
            ApiTestParam apiTestParam = new ApiTestParam();
            for(Map.Entry entry : params.entrySet()){
                System.out.println("----------->key:"+entry.getKey()+"value:"+entry.getValue());
                if(entry.getKey().equals("apiParamKey")){
                    apiTestParam.setApiParamKey(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamValue")){
                    apiTestParam.setApiParamValue(String.valueOf(entry.getValue()));
                }
            }
            System.out.println("-----> " + apiTestParam);
            apiParamArrayList.add(apiTestParam);
        }

        return measureUserService.performanceTest(url,apiParamArrayList,request);
    }

    @PostMapping("/pass")
    public ReturnData pass(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return measureUserService.passTest(userId, apiId,request);
    }

    @PostMapping("/unPass")
    public ReturnData unPass(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return measureUserService.unPassTest(userId, apiId,request);
    }
}
