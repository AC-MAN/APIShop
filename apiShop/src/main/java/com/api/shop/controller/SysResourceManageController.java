package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.ApiMessage;
import com.api.shop.bean.ApiParam;
import com.api.shop.bean.ApiVO;
import com.api.shop.common.ApiStatusEnum;
import com.api.shop.common.ReturnData;
import com.api.shop.service.AuditUserService;
import com.api.shop.service.GeneralUserService;
import com.api.shop.service.MeasureUserService;
import com.api.shop.service.SysResourceManageService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Map;

/**
 * @Auther: wyx
 * @Date: 2019-04-10 10:35
 * @Description: 超级管理员对资源的管理 Controller
 */
@RestController
@RequestMapping("/admin/resource")
public class SysResourceManageController {

    @Resource
    private SysResourceManageService sysResourceManageService;
    @Resource
    private GeneralUserService generalUserService;
    @Resource
    private AuditUserService auditUserService;
    @Resource
    private MeasureUserService measureUserService;

    /**
     *  超级管理员定义 API
     */
    @PostMapping(value = "/addAPI")
    public ReturnData addAPI(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        ApiMessage apiMessage = new ApiMessage();
        String apiUrl = jsonObject.getString("apiUrl");
        String apiType = jsonObject.getString("apiType");
        String apiDescription = jsonObject.getString("apiDescription");
        apiMessage.setApiUrl(apiUrl);
        apiMessage.setApiType(apiType);
        apiMessage.setApiDescription(apiDescription);

        System.out.println(jsonObject.get("apiParams"));
        ArrayList<Map<String,String>> maps = (ArrayList<Map<String,String>>) jsonObject.get("apiParams");
        ArrayList<ApiParam> apiParamArrayList = new ArrayList<>();
        for(Map<String,String> params : maps){
            ApiParam apiParam = new ApiParam();
            for(Map.Entry entry : params.entrySet()){
                System.out.println("----------->key:"+entry.getKey()+"value:"+entry.getValue());
                if(entry.getKey().equals("apiParamKey")){
                    apiParam.setApiParamKey(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamValue")){
                    apiParam.setApiParamValue(String.valueOf(entry.getValue()));
                } else if(entry.getKey().equals("apiParamType")){
                    apiParam.setApiParamType(String.valueOf(entry.getValue()));
                }
            }
            System.out.println("-----> " + apiParam);
            apiParamArrayList.add(apiParam);
        }

        return generalUserService.addAPI(apiMessage, apiParamArrayList, request);
    }

    /**
     *  超级管理员修改 API 分类
     */
    @PostMapping(value = "/classifyAPI")
    public ReturnData classifyAPI(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        String type = jsonObject.getString("type");
        return sysResourceManageService.classifyAPI(userId, apiId, type, request);
    }

    /**
     *  超级管理员删除 API
     */
    @PostMapping(value = "/deleteAPI")
    public ReturnData deleteAPI(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return generalUserService.deleteAPI(userId, apiId, request);
    }

    /**
     *  超级管理员批量删除 API
     */
    @PostMapping(value = "/batchDeleteAPI")
    public ReturnData batchDeleteAPI(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        ArrayList<Map<String,Integer>> apiMapList = (ArrayList<Map<String,Integer>>)jsonObject.get("apiIds");
        ArrayList<ApiVO> apiVOArrayList = new ArrayList<>();
        for(Map<String,Integer> map: apiMapList){

           ApiVO apiVO = new ApiVO(map.get("apiId"),map.get("userId"));
            apiVOArrayList.add(apiVO);
        }
        return sysResourceManageService.batchDeleteAPI(apiVOArrayList, request);
    }

    /**
     *  显示所有 API
     */
    @PostMapping(value = "/showAllAPI")
    public ReturnData showAllAPI(HttpServletRequest request){
        return generalUserService.searchAllAPI(request);
    }

    /**
     *  显示某一状态下的所有 API
     */
    @PostMapping(value = "/showAPIByStatus")
    public ReturnData showAPIByStatus(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer status = jsonObject.getInteger("status");
        return sysResourceManageService.showAPIByStatus(status, request);
    }

    /**
     *  显示某一分类的所有 API
     */
    @PostMapping(value = "/showAPIByType")
    public ReturnData showAPIByType(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String type = jsonObject.getString("type");
        return sysResourceManageService.showAPIByType(type, request);
    }

    @PostMapping(value = "/showAllType")
    public ReturnData showAllType(HttpServletRequest request){
        return generalUserService.showAllType(request);
    }

    /**
     * Api审核通过
     */
    @PostMapping(value = "/passAuditApi")
    public ReturnData passAuditApi(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return auditUserService.passAuditApi(userId, apiId, request);
    }

    /**
     * Api审核不通过
     */
    @PostMapping(value = "/unPassAuditApi")
    public ReturnData unPassAuditApi(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return auditUserService.unPassAuditApi(userId, apiId, request);
    }

    /**
     * 测试通过
     */
    @PostMapping(value = "/passTest")
    public ReturnData passTest(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return measureUserService.passTest(userId, apiId, request);
    }

    /**
     * 测试不通过
     */
    @PostMapping(value = "/unPassTest")
    public ReturnData unPassTest(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return measureUserService.unPassTest(userId, apiId, request);
    }

}
