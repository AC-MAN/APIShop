package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.SysUser;
import com.api.shop.common.ReturnData;
import com.api.shop.exception.ParamException;
import com.api.shop.service.SysUserManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/3 14:58
 * @Description: 超级用户管理用户Controller
 */

@RestController
@RequestMapping("/admin/user")
public class SysUserManageController {

    @Resource
    private SysUserManageService sysUserManageService;

    @PostMapping("/showAll")
    public ReturnData showAll(HttpServletRequest request){

        return sysUserManageService.showAll(request);
    }
    /**
     * 显示用户信息byID
     * @return
     */
    @PostMapping("/show")
    public ReturnData showUser(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        return sysUserManageService.showUser(userId,request);
    }

    /**
     * 管理员手动添加用户
     * @return
     */

    @PostMapping("register")
    public ReturnData register(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        SysUser sysUser = new SysUser();
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        String nickName = jsonObject.getString("nickName");
        sysUser.setUsername(username);
        sysUser.setPassword(password);
        sysUser.setNickName(nickName);
        return sysUserManageService.registerUser(sysUser, request);
    }

    /**
     * 删除用户信息
     * @return
     */
    @PostMapping("delete")
    public ReturnData delete(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        return sysUserManageService.deleteUser(userId,request);
    }

    /**
     * 更新用户信息
     * @return
     */
    @PostMapping("update")
    public ReturnData update(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId  = jsonObject.getInteger("userId");
        String nikeName = jsonObject.getString("nickName");
        return sysUserManageService.updateUser(userId,nikeName,request);
    }

}
