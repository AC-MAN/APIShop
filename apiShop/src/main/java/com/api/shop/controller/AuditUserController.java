package com.api.shop.controller;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.common.ApiStatusEnum;
import com.api.shop.common.ReturnData;
import com.api.shop.service.AuditUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/5 21:01
 * @Description: 审核用户Controller
 */
@RestController
@RequestMapping("/audit")
public class AuditUserController {

    @Resource
    private AuditUserService auditUserService;

    @PostMapping("/get")
    public ReturnData getUnAudit(HttpServletRequest request){
        Integer status = ApiStatusEnum.CHECK_PENDING.getValue();
        return auditUserService.getApiByStatus(status,request);
    }

    @PostMapping("/pass")
    public ReturnData pass(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return auditUserService.passAuditApi(userId, apiId,request);
    }

    @PostMapping("/unPass")
    public ReturnData unPass(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        Integer userId = jsonObject.getInteger("userId");
        Integer apiId = jsonObject.getInteger("apiId");
        return auditUserService.unPassAuditApi(userId, apiId,request);
    }
}
