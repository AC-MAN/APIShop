package com.api.shop.common;

import com.api.shop.bean.SysUser;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 当前线程保存的信息
 */

public class RequestHolder {

    private static final ThreadLocal<SysUser> userHolder = new ThreadLocal<>();

    private static final ThreadLocal<String> tokenHolder = new ThreadLocal<>();

    private static final ThreadLocal<String> ipHolder = new ThreadLocal<>();

    public static void add(SysUser sysUser){
        userHolder.set(sysUser);
    }

    public static void add(String token){
        tokenHolder.set(token);
    }

    public static void addIp(String ip) {ipHolder.set(ip);}

    public static SysUser getCurrentUser(){
        return userHolder.get();
    }

    public static String getCurrentToken(){
        return tokenHolder.get();
    }

    public static String getCurrentIp(){  return ipHolder.get();}

    public static void remove(){
        userHolder.remove();
        tokenHolder.remove();
        ipHolder.remove();
    }

    public static void removeUser(){
        userHolder.remove();
    }

    public static void removeToken(){
        tokenHolder.remove();
    }

}
