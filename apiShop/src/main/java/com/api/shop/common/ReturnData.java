package com.api.shop.common;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 封装返回值
 */

@Getter
@Setter
public class ReturnData {

    private boolean result;

    private String message;

    private Object data;

    public ReturnData(boolean result){
        this.result = result;
    }

    public static ReturnData success(Object object){
        ReturnData returnData = new ReturnData(true);
        returnData.setData(object);
        return returnData;
    }

    public static ReturnData success(){
        return new ReturnData(true);
    }

    public static ReturnData fail(String message){
        ReturnData returnData = new ReturnData(false);
        returnData.setMessage(message);
        return returnData;
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("result", this.result);
        result.put("message", message);
        result.put("data", data);
        return result;
    }

}
