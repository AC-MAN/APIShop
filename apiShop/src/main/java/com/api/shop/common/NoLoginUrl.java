package com.api.shop.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 21:13
 * @Description: 用户无需登录便可以访问的 URL
 */

public class NoLoginUrl {

    private static Map<String, Object> map= new HashMap<>();

    static {
        map.put("registerUrl", "/apiShop/user/register");
        map.put("loginUrl", "/apiShop/user/login");
        map.put("sendSMSUrl","/apiShop/user/sendSMS");
        map.put("qkLoginUrl","/apiShop/user/qkLogin");
        map.put("showAllAPI","/apiShop/admin/resource/show");

        map.put("localRegisterUrl", "/user/register");
        map.put("localLoginUrl", "/user/login");
        map.put("localSendSMSUrl","/user/sendSMS");
        map.put("localQkLoginUrl","/user/qkLogin");
    }

    public static void add(String key, String url){
        map.put(key, url);
    }

    public static void remove(String key){ map.remove(key);}

    public static Map<String, Object> getMap() {
        return map;
    }
}
