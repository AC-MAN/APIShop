package com.api.shop.common;

/**
 * @Auther: wyx
 * @Date: 2019-04-04 16:20
 * @Description: API 状态对应表
 */
public enum  ApiStatusEnum {

    NORMAL(0),           //正常使用
    CHECK_PENDING(1),    //待审核
    TO_BE_TESTED(2),     //待测试
    AUDIT_FAILURE(-1),   //审核不合格
    TEST_FAILURE(-2);    //测试不合格

    private int value;

    ApiStatusEnum(int value){
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }

}
