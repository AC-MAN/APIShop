package com.api.shop.util;

import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class MD5Util {

    public static String getEncoderStr(String psw){
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        }catch (NoSuchAlgorithmException e){
            log.error("md5 get error: {} ", e.getMessage());
        }
        BASE64Encoder base64Encoder = new BASE64Encoder();
        try {
            String enStr = base64Encoder.encode(md5.digest(psw.getBytes("utf-8")));
            return enStr;
        }catch (Exception e){
            log.error("password format error: {} ", e.getMessage());
        }
        return null;
    }

}
