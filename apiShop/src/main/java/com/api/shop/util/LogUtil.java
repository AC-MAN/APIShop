package com.api.shop.util;

import com.api.shop.bean.SysLog;
import com.api.shop.common.RequestHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/9 10:27
 * @Description: 日志记录工具类
 */
public class LogUtil {

    /**
     *
     * @param result 操作结果（成功/失败)
     * @return
     */
    public static SysLog setLog(String detailMessage, Boolean result, String operateName, String ip){

        SysLog sysLog = new SysLog();
        Integer operatorId = 0;
//        if(!(operateName.equals("登录")||operateName.equals("login"))){
            operatorId = RequestHolder.getCurrentUser().getUserId();
//        }

        String message = null;
        String boolMessage= result?"操作成功":"操作失败";
        message = boolMessage+"," + detailMessage;
        sysLog.setLogOperatorIp(ip);
        sysLog.setLogMessage(message);
        sysLog.setLogDateTime(new Date());
        sysLog.setLogOperate(operateName);
        sysLog.setLogOperatorId(operatorId);
        return sysLog;

    }
}
