package com.api.shop.util;

import com.api.shop.dao.SysLogDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author: 杨俊杰
 * @Date: 2019/4/16 21:47
 * @Description: 定时任务类
 */
@Component
@Slf4j
public class ScheduledTasks {

    @Resource
    SysLogDao sysLogDao;

    /**
     * 一天清理一次日志
     */
    @Scheduled(fixedRate = 1000*60*60*24)
    public void clearLog(){

        log.info(System.currentTimeMillis()+"日志清除完毕------------------");
        sysLogDao.clearLog();
    }
}
