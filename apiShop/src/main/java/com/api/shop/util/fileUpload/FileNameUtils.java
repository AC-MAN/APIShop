package com.api.shop.util.fileUpload;

import com.api.shop.util.UUIDUtils;

/**
 * @Auther: wyx
 * @Date: 2019-04-09 12:13
 * @Description: 为图片生成新的文件名
 */
public class FileNameUtils {

    /**
     * 获取文件后缀
     * @param fileName
     * @return
     */
    public static String getSuffix(String fileName){
        return fileName.substring(fileName.lastIndexOf("."));
    }

    /**
     * 生成新的文件名
     * @param fileOriginName 源文件名
     * @return
     */
    public static String getFileName(String fileOriginName){
        return UUIDUtils.getUUID() + FileNameUtils.getSuffix(fileOriginName);
    }

}
