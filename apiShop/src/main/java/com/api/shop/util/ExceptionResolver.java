package com.api.shop.util;

import com.api.shop.common.ReturnData;
import com.api.shop.exception.ParamException;
import com.api.shop.exception.PermissionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理类
 */
@ControllerAdvice
@Slf4j
public class ExceptionResolver {

    @ResponseBody
    @ExceptionHandler(value = ParamException.class)
    public ReturnData ParamExceptionHandler(ParamException e){
        log.error("---> ParamException : {}",  e.getMessage());
        return ReturnData.fail(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = PermissionException.class)
    public ReturnData PermissionExceptionHandler(PermissionException e){
        log.error("---> PermissionException : {}", e.getMessage());
        return ReturnData.fail(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ReturnData Exception(Exception e){
        log.error("---> Exception : {}", e.getMessage());
        return ReturnData.fail(e.getMessage());
    }
}
