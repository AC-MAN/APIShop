package com.api.shop.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyTokenUtil {

    public static String createToken(String ip, String username){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(username);
        stringBuffer.append("_");
        stringBuffer.append(ip);
        stringBuffer.append("_");
        stringBuffer.append(format.format(new Date()));
        return stringBuffer.toString();
    }

}
