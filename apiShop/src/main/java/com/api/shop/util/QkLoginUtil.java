package com.api.shop.util;

import com.alibaba.fastjson.JSONObject;
import com.api.shop.bean.SysUser;
import com.api.shop.exception.ParamException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/10 15:28
 * @Description:
 */
@Slf4j
public class QkLoginUtil {

    private static final String apiKey = "XtsBim169025efcb6597ba3192bf4c2fdc40b4ce2c95d56";
    private static final String url  = "https://api.apishop.net/communication/sms/send";
    private static final String templateID = "10329";

    private static String proxToDesURL(String url,String method, Map<String,String> params){

            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            RestTemplate restTemplate = new RestTemplate(requestFactory);
            HttpHeaders requestHeaders = new HttpHeaders();
            //TODO 加入请求头
            MultiValueMap<String,String> parmList = new LinkedMultiValueMap<>();
            if(params!=null && !params.isEmpty()){
                if(method.equalsIgnoreCase("GET")){
                url+="?";
                Set<String> set = params.keySet();
                for(Iterator<String> iterator = set.iterator();iterator.hasNext();){

                    String key = iterator.next();
                    String param = params.get(key);
                    url += key+"="+param+"&";
                }
                url = url.substring(0,url.length()-1);
            }
                else{
                    Set<String> set = params.keySet();
                    for(Iterator<String> iterator = set.iterator();iterator.hasNext();){

                        String key = iterator.next();
                        String param = params.get(key);
                        parmList.add(key,param);
                    }
                }
        }
            requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            HttpEntity<MultiValueMap<String,String>> requestEntity = new HttpEntity<>(parmList,requestHeaders);
            //处理请求方法
            HttpMethod requestType;
            method = method.toUpperCase();
            switch (method) {
                case "GET":
                    requestType = HttpMethod.GET;
                    break;
                case "POST":
                    requestType = HttpMethod.POST;
                    break;
                case "PUT":
                    requestType = HttpMethod.PUT;
                    break;
                case "DELETE":
                    requestType = HttpMethod.DELETE;
                    break;
                case "HEAD":
                    requestType = HttpMethod.HEAD;
                    break;
                case "OPTIONS":
                    requestType = HttpMethod.OPTIONS;
                    break;
                default:
                    requestType = HttpMethod.GET;
                    break;
            }
            ResponseEntity<String> responseEntity = restTemplate.exchange(url,requestType,requestEntity,String.class,params);
            return responseEntity.getBody();

    }

    public static String sendSMS(String phoneNumber,String param){

    String requestMethod = "POST";
    Map<String,String> params = new HashMap<>();
    params.put("apiKey",apiKey);
    params.put("phoneNum",phoneNumber);
    params.put("templateID",templateID);
    params.put("params",param);
    String result = proxToDesURL(url,requestMethod,params);
        if (result != null) {
            JSONObject jsonObject = JSONObject.parseObject(result);
            String status_code = jsonObject.getString("statusCode");
            if (status_code.equals("000000")) {
                // 状态码为000000, 说明请求成功
                log.info("请求成功：" + jsonObject.getString("result"));
                return result;
            } else {
                // 状态码非000000, 说明请求失败
                throw new ParamException("请求短信接口失败");
            }
        } else {

            throw new ParamException("短信接口返回内容异常");
        }

    }

    //生成六位随机数
    public static synchronized  String KeyParam(){

    return String.valueOf((int)((Math.random()*9+1)*1000000));
    }

    public static SysUser setPhoneUser(String phoneNumber){

        SysUser sysUser = new SysUser();
        sysUser.setUsername(phoneNumber);
        sysUser.setPassword(MD5Util.getEncoderStr(phoneNumber+"PSW"));
        sysUser.setNickName("@phone_"+phoneNumber);
        sysUser.setAvatar("http://yjjvip.cn:8080/pictures/default.jpg");
        return sysUser;
    }
}
