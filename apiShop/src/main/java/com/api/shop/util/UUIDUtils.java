package com.api.shop.util;

import java.util.UUID;

/**
 * @Auther: wyx
 * @Date: 2019-04-09 12:12
 * @Description: 生成随机字符串
 */
public class UUIDUtils {

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

}
