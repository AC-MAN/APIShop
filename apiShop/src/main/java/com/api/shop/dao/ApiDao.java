package com.api.shop.dao;

import com.api.shop.bean.ApiMessage;
import com.api.shop.bean.ApiParam;
import org.apache.ibatis.annotations.*;

import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 23:13
 * @Description: API 相关数据库操作
 */

@Mapper
public interface ApiDao {

    String TABLE_NAME_MESSAGE = "api_message";
    String INSERT_FIELDS_MESSAGE = "api_url, api_type, api_status, api_belong_user_id, api_description";
    String SELECT_FIELDS_MESSAGE = "api_id apiId, api_url apiUrl, api_type apiType," +
            " api_status apiStatus, api_belong_user_id apiBelongUserId, api_description apiDescription";

    String TABLE_NAME_PARAM = "api_param";
    String INSERT_FIELDS_PARAM = "api_id, api_param_key, api_param_value, api_param_type";
    String SELECT_FIELDS_PARAM = "param_id paramId, api_id apiId, api_param_key apiParamKey," +
            " api_param_value apiParamValue, api_param_type apiParamType";

    @Insert({"insert into ", TABLE_NAME_MESSAGE, " (", INSERT_FIELDS_MESSAGE, ") values( " +
            "#{apiUrl}, #{apiType}, #{apiStatus}, #{apiBelongUserId}, #{apiDescription})"})
    @Options(useGeneratedKeys = true, keyProperty = "apiId", keyColumn = "api_id")
    int insertMessage(ApiMessage apiMessage);

    @Insert("<script>"
            + "insert into " + TABLE_NAME_PARAM + " (" + INSERT_FIELDS_PARAM + ") values"
            + "<foreach item='param' collection='apiParamArrayList' separator=','>"
                + "(#{apiId}, #{param.apiParamKey}, #{param.apiParamValue}, #{param.apiParamType})"
            + "</foreach>"
            + "</script>")
    int insertParam(@Param("apiParamArrayList") ArrayList<ApiParam> apiParamArrayList, @Param("apiId") Integer apiId);

    @Delete({"delete from ", TABLE_NAME_MESSAGE, " where api_id = #{apiId}"})
    int deleteMessage(@Param("apiId") Integer apiId);

    @Delete({"delete from ", TABLE_NAME_PARAM, " where api_id = #{apiId}"})
    int deleteAllParam(@Param("apiId") Integer apiId);

    @Delete({"delete from ", TABLE_NAME_PARAM, " where param_id = #{paramId}"})
    int deleteParam(@Param("paramId") Integer paramId);

    @Select({"select ", SELECT_FIELDS_MESSAGE, " from ", TABLE_NAME_MESSAGE, " where api_id = #{apiId}"})
    ApiMessage selectMessage(@Param("apiId") Integer apiId);

    @Select({"select ", SELECT_FIELDS_MESSAGE, " from ", TABLE_NAME_MESSAGE, " where api_url = #{apiUrl}"})
    ApiMessage selectMessageByUrl(@Param("apiUrl") String apiUrl);

    @Select({"select ", SELECT_FIELDS_MESSAGE, " from ", TABLE_NAME_MESSAGE})
    ArrayList<ApiMessage> selectAllApiMessage();

    @Select({"select ", SELECT_FIELDS_MESSAGE, " from ", TABLE_NAME_MESSAGE, " where api_belong_user_id = #{userId}"})
    ArrayList<ApiMessage> selectUserApiMessage(@Param("userId") Integer userId);

    @Select({"select ", SELECT_FIELDS_MESSAGE, " from ", TABLE_NAME_MESSAGE, " where api_belong_user_id = #{userId} and api_status = #{apiStatus}"})
    ArrayList<ApiMessage> selectUserApiMessageByStatus(@Param("userId") Integer userId, @Param("apiStatus") Integer apiStatus);

    @Select({"select ", SELECT_FIELDS_MESSAGE, "from ", TABLE_NAME_MESSAGE, " where api_status = #{status}"})
    ArrayList<ApiMessage> selectApiMessageByStatus(@Param("status") Integer status);

    @Select({"select ", SELECT_FIELDS_MESSAGE, " from ", TABLE_NAME_MESSAGE, " where api_belong_user_id = #{userId} and api_type = #{apiType}"})
    ArrayList<ApiMessage> selectUserApiMessageByType(@Param("userId") Integer userId, @Param("apiType") String apiType);

    @Select({"select ", SELECT_FIELDS_PARAM, " from ", TABLE_NAME_PARAM, " where api_id = #{apiId}"})
    ArrayList<ApiParam> selectParams(@Param("apiId") Integer apiId);

    @Update({"update ", TABLE_NAME_MESSAGE, " set api_url = #{apiUrl}, api_type = #{apiType}, api_description = #{apiDescription} where api_id = #{apiId}"})
    int updateMessage(ApiMessage apiMessage);

    @Update({"update ", TABLE_NAME_MESSAGE, " set api_status = #{status} where api_id = #{apiId}"})
    int updateStatus(@Param("apiId") Integer apiId, @Param("status") Integer status);

    @Update({"update ", TABLE_NAME_PARAM, " set api_id = #{apiId}, api_param_key = #{apiParamKey}, api_param_value = #{apiParamValue}, " +
            "api_param_type = #{apiParamType} where param_id = #{paramId}"})
    int updateParam(ApiParam apiParam);

    @Update({"update ", TABLE_NAME_MESSAGE, " set api_type = #{type} where api_id = #{apiId}"})
    int updateType(@Param("apiId") Integer apiId, @Param("type") String type);

    @Select({"select ", SELECT_FIELDS_MESSAGE, " from ", TABLE_NAME_MESSAGE, " where api_type = #{apiType}"})
    ArrayList<ApiMessage> selectApiMessageByType(@Param("apiType") String apiType);

    @Select({"select api_type from ", TABLE_NAME_MESSAGE, " where api_status = 0"})
    ArrayList<String> selectApiTypes();

}
