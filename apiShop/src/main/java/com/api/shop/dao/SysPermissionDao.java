package com.api.shop.dao;

import com.api.shop.bean.SysPermission;
import org.apache.ibatis.annotations.*;

import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 23:13
 * @Description: 权限相关数据库操作
 */

@Mapper
public interface SysPermissionDao {

    String TABLE_NAME = "sys_permission";
    String INSERT_FIELDS = "permission_name, permission_description, permission_status, permission_url";
    String SELECT_FIELDS = "permission_id permissionId, permission_name permissionName," +
            " permission_description permissionDescription, permission_status permissionStatus, permission_url permissionUrl";

    @Insert({"insert into ", TABLE_NAME, " (", INSERT_FIELDS, ") values(#{permissionName}, " +
            "#{permissionDescription}, #{permissionStatus}, #{permissionUrl})"})
    int insert(SysPermission sysPermission);

    @Delete({"delete from ", TABLE_NAME, " where permission_id = #{permissionId}"})
    int delete(@Param("permissionId") Integer permissionId);

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME,
            " where permission_id = #{permissionId}"})
    SysPermission selectById(@Param("permissionId") Integer permissionId);

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME})
    ArrayList<SysPermission> selectAll();

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME, " where permission_url like #{permissionUrl}"})
    ArrayList<SysPermission> selectByUrl(@Param("permissionUrl") String permissionUrl);

    @Select({"select permission_id from ", TABLE_NAME, " where permission_url like #{permissionUrl}"})
    ArrayList<Integer> selectIdsByUrl(@Param("permissionUrl") String permissionUrl);

    /**
     * 冻结（停止使用）该角色
     */
    @Update({"update ", TABLE_NAME, " set status = 0 where permission_id = #{permissionId}"})
    int freeze(@Param("permissionId") Integer permissionId);

    /**
     * 激活该角色
     */
    @Update({"update ", TABLE_NAME, " set status = 1 where permission_id = #{permissionId}"})
    int  activate(@Param("permissionId") Integer permissionId);

    @Update({"update ", TABLE_NAME, " set permission_name = #{permissionName}, permission_url = #{permissionUrl}, permission_description = #{permissionDescription}, permission_status = " +
            "#{permissionStatus} where permission_id = #{permissionId}"})
    int update(SysPermission sysPermission);

}
