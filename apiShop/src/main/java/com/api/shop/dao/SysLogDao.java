package com.api.shop.dao;
import com.api.shop.bean.SysLog;
import org.apache.ibatis.annotations.*;
import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 23:13
 * @Description: 日志相关数据库操作
 */

@Mapper
public interface SysLogDao {

    String TABLE_NAME = "sys_log";
    String INSERT_FIELDS = "log_datetime, log_operator_id, log_operator_ip, log_operate, log_message";
    String SELECT_FIELDS = "log_id logId, log_datetime logDateTime, log_operator_id logOperatorId," +
            " log_operator_ip logOperatorIp, log_operate logOperate, log_message logMessage";

    @Insert({"insert into ", TABLE_NAME, "(", INSERT_FIELDS,")values(#{logDateTime}, " +
            "#{logOperatorId}, #{logOperatorIp}, #{logOperate}, #{logMessage})"})
    int insert(SysLog sysLog);

    @Delete({"delete from ", TABLE_NAME, " where log_id = #{logId}"})
    int delete(@Param("logId") Integer logId);

    @Delete({"delete from", TABLE_NAME })
    void clearLog();
    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME, " where log_id = #{logId}"})
    SysLog selectById(@Param("logId") Integer logId);

    @Select({"select ",SELECT_FIELDS," from " ,TABLE_NAME ," where log_operate = #{logOperate}"})
    ArrayList<SysLog> selectByOperate(@Param("logOperate") String logOperate);

    @Select({"select ",SELECT_FIELDS," from ", TABLE_NAME," where log_operator_id = #{logOperatorId}"})
    ArrayList<SysLog> selectByOperatorId(Integer operatorId);

    @Select({"select ", SELECT_FIELDS," from ", TABLE_NAME})
    ArrayList<SysLog> selectAll();

}
