package com.api.shop.dao;

import org.apache.ibatis.annotations.*;

import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 23:13
 * @Description: 用户角色相关数据库操作
 */

@Mapper
public interface UserRoleDao {

    String TABLE_NAME = "user_role";
    String INSERT_FIELDS = "user_id, role_id";
    String SELECT_FIELDS = "user_role_id userRoleId, user_id userId, role_id roleId";

    @Insert({"insert into ", TABLE_NAME, " (", INSERT_FIELDS, ") values(#{userId}, #{roleId})"})
    int insertIntoRole(@Param("userId") Integer userId, @Param("roleId") Integer roleId);

    @Select({"select role_id from ", TABLE_NAME, " where user_id = #{userId}"})
    ArrayList<Integer> selectRoleIdListByUserId(@Param("userId") Integer userId);

    @Delete({"delete from ", TABLE_NAME, " where role_id = #{roleId}"})
    int deleteByRoleId(@Param("roleId") Integer roleId);

    @Delete({"delete from ", TABLE_NAME, " where user_id = #{userId}"})
    int deleteByUserId(@Param("userId") Integer userId);

    @Update({"update ",TABLE_NAME," set role_id =#{roleId} where user_id=#{userId}"})
    int update(@Param("roleId")Integer roleId, @Param("userId")Integer userId);

}
