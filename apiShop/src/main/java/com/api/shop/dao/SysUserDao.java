package com.api.shop.dao;

import com.api.shop.bean.SysUser;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 23:13
 * @Description: 用户相关数据库操作
 */

@Mapper
public interface SysUserDao {

    String TABLE_NAME = "sys_user";
    String INSERT_FIELDS = "username, password, nick_name, avatar";
    String SELECT_FIELDS = "user_id userId, username, password, nick_name nickName, avatar";

    @Insert({"insert into ", TABLE_NAME, " (", INSERT_FIELDS, ") values(#{username}, " +
            "#{password}, #{nickName}, #{avatar})"})
    @Options(useGeneratedKeys = true, keyProperty = "userId", keyColumn = "user_id")
    int insert(SysUser sysUser);

    @Delete({"delete from ", TABLE_NAME, " where user_id = #{userId}"})
    int delete(@Param("userId") Integer userId);

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME, " where user_id = #{userId}"})
    SysUser selectUserById(@Param("userId") Integer userId);

    @Select({"select  ", SELECT_FIELDS ," from ",TABLE_NAME})
    ArrayList<SysUser> selectAll();

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME, " where username = #{username}"})
    SysUser selectUserByName(@Param("username") String username);

    @Update({"update ", TABLE_NAME, " set nick_name = #{nickName} where user_id = #{userId}"})
    int update(SysUser sysUser);

    @Update({"update ", TABLE_NAME ," set avatar = #{avatar} where user_id = ${userId}"})
    int upAvatar(SysUser sysUser);

}
