package com.api.shop.dao;

import com.api.shop.bean.SysRole;
import org.apache.ibatis.annotations.*;

import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-02 23:13
 * @Description: 角色相关数据库操作
 */

@Mapper
public interface SysRoleDao {

    String TABLE_NAME = "sys_role";
    String INSERT_FIELDS = "role_name, role_description, role_status";
    String SELECT_FIELDS = "role_id roleId, role_name roleName, " +
            "role_description roleDescription, role_status roleStatus";

    @Insert({"insert into ", TABLE_NAME, " (", INSERT_FIELDS, ") values(#{roleName}, " +
            "#{roleDescription}, #{roleStatus})"})
    int insert(SysRole sysRole);

    @Delete({"delete from ", TABLE_NAME, " where role_id = #{roleId}"})
    int delete(@Param("roleId") Integer roleId);

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME, " where role_id = #{roleId}"})
    SysRole selectByRoleId(@Param("roleId") Integer roleId);

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME})
    ArrayList<SysRole> selectAllRole();

    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME, " where role_name = #{roleName}"})
    SysRole selectByRoleName(@Param("roleName") String roleName);

    @Select("<script>"
            + " select " + SELECT_FIELDS + " from " + TABLE_NAME
            + " where role_id in ("
            + "<foreach collection='roleIdList' item='roleId' separator=','>"
                + "#{roleId}"
            + "</foreach>"
            + ")"
            + "</script>")
    ArrayList<SysRole> selectRolesByIdList(@Param("roleIdList") ArrayList<Integer> roleIdList);

    @Select("<script>"
            + " select role_name from " + TABLE_NAME
            + " where role_id in ("
            + "<foreach collection='roleIdList' item='roleId' separator=','>"
            + "#{roleId}"
            + "</foreach>"
            + ")"
            + "</script>")
    ArrayList<String> selectRoleNameByIdList(@Param("roleIdList") ArrayList<Integer> roleIdList);

    /**
     * 冻结（停止使用）该角色
     * @param roleId    角色 id
     * @return    操作行数
     */
    @Update({"update ", TABLE_NAME, " set role_status = 0 where role_id = #{roleId}"})
    int freeze(@Param("roleId") Integer roleId);

    /**
     * 激活该角色
     * @param roleId    角色 id
     * @return    操作行数
     */
    @Update({"update ", TABLE_NAME, " set role_status = 1 where role_id = #{roleId}"})
    int  activate(@Param("roleId") Integer roleId);

    @Update({"update ", TABLE_NAME, " set role_name = #{roleName}, role_description = #{roleDescription}, role_status = #{roleStatus} where role_id = #{roleId}"})
    int update(SysRole sysRole);

}
