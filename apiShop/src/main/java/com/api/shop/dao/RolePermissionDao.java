package com.api.shop.dao;

import org.apache.ibatis.annotations.*;

import java.util.ArrayList;

/**
 * @Auther: wyx
 * @Date: 2019-04-04 14:06
 * @Description: 角色权限相关数据库操作
 */
@Mapper
public interface RolePermissionDao {

    String TABLE_NAME = "role_permission";
    String INSERT_FIELDS = "role_id, permission_id";
    String SELECT_FIELDS = "role_permission_id rolePermissionId, role_id roleId, permission_id permissionId";

    @Insert({"insert into ", TABLE_NAME, " (", INSERT_FIELDS, ") values(#{roleId}, #{permissionId})"})
    int insert(@Param("roleId") Integer roleId, @Param("permissionId") Integer permissionId);

    @Select({"select role_id from ", TABLE_NAME, " where permission_id = #{permissionId}"})
    ArrayList<Integer> selectRoleIdByPermissionId(@Param("permissionId") Integer permissionId);

    @Select("<script>"
            + " select role_id from " + TABLE_NAME + " where permission_id in ("
            + "<foreach collection='permissionIdList' item='permissionId' separator=','>"
            + "#{permissionId}"
            + "</foreach>"
            + ")"
            + "</script>")
    ArrayList<Integer> selectRoleIdByPermissionIdList(@Param("permissionIdList") ArrayList<Integer> permissionIdList);

    @Select({"select permission_id from ", TABLE_NAME, " where role_id = #{roleId}"})
    ArrayList<Integer> selectPermissionIdByRoleId(@Param("roleId") Integer roleId);

    @Delete({"delete from ", TABLE_NAME, " where permission_id = #{permissionId}"})
    int delete(@Param("permissionId") Integer permissionId);

}
