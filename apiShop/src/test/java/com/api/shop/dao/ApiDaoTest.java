package com.api.shop.dao;

import com.api.shop.bean.ApiMessage;
import com.api.shop.bean.ApiParam;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/5 20:35
 * @Description:
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ApiDaoTest {

    @Resource
    private ApiDao apiDao;

//    @Test
//    public void test1(){
//
//        ApiMessage apiMessage = apiDao.selectMessage(1);
//        ArrayList<ApiParam> apiParamArrayList = apiDao.selectParams(apiMessage.getApiId());
//        apiMessage.setApiParamArrayList(apiParamArrayList);
//        Assert.assertNotNull(apiMessage);
//    }

    @Test
    public void test2(){

        ArrayList<ApiMessage> apiMessageArrayList = apiDao.selectApiMessageByStatus(1);
        Assert.assertNotEquals(0,apiMessageArrayList.size());
    }

//    @Test
//    public void test3(){
//
//        ApiMessage apiMessage = apiDao.selectMessageByUrl("http://127.0.0.1:8080/test/test1");
//        Assert.assertNotNull(apiMessage);
//    }
}