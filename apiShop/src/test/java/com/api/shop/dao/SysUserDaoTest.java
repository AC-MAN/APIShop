package com.api.shop.dao;

import com.api.shop.bean.SysUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: 杨俊杰
 * @Date: 2019/4/16 09:44
 * @Description:
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class SysUserDaoTest {

    @Resource
    private SysUserDao sysUserDao;

    @Test
    public void test1(){

        ArrayList<SysUser> userArrayList = sysUserDao.selectAll();
        Assert.assertNotEquals(userArrayList.size(),0);
    }

}