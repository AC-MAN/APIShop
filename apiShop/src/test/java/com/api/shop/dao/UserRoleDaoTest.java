package com.api.shop.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/4 14:46
 * @Description:
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRoleDaoTest {

    @Resource
    UserRoleDao userRoleDao;

    @Test
    @Transactional
    @Rollback
    public void test1(){

        userRoleDao.deleteByUserId(11);
    }
}