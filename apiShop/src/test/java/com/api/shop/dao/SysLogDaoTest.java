package com.api.shop.dao;

import com.api.shop.bean.SysLog;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Auther: 杨俊杰
 * @Date: 2019/4/8 15:43
 * @Description:
 */

@SpringBootTest
@RunWith(SpringRunner.class)
public class SysLogDaoTest {


    @Resource
    SysLogDao sysLogDao;

    @Test
    public void test1(){

        ArrayList<SysLog> sysLogArrayList = sysLogDao.selectByOperate("api测试通过");
        Assert.assertNotNull(sysLogArrayList);
    }

    @Test
    public void test2(){

        sysLogDao.clearLog();
    }
}